package wka.com.wardkonnect;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileView extends AppCompatActivity {


    RelativeLayout rel_followers;
    RelativeLayout rel_following;


    Button followButton;


    TextView my_profile_followers_count;
    TextView my_profile_following_count;
    ImageView my_profile_profile_pic;

    TextView my_profile_name;
    TextView my_profileLocation;
    TextView my_profile_profilebio;
    TextView my_profileLink;

    ImageButton message;


    String str_id;
    String str_reg_id;
    String str_username;
    String str_name;
    String str_location;
    String str_profession;
    String str_region;
    String str_url;
    String str_followersct;
    String str_followingct;
    String str_postsct;
    String str_status;
    String str_profile_picture;
    String str_profile_banner;
    String str_profile_verified;

    static String PROFILE = "profile";
    static String PROFILE_ID = "id";
    static String PROFILE_STATUS = "status";
    static String PROFILE_FOLLOWERS = "followers_count";
    static String PROFILE_FOLLOWING = "following_count";
    static String PROFILE_POSTS = "posts_count";
    static String PROFILE_PICTURE = "profile_picture";
    static String PROFILE_BANNER = "profile_banner";

    JSONObject jsonobject;
    JSONArray jsonarray;
    ArrayList<HashMap<String, String>> arraylist;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    ImageButton back;

    public static final String TAG = "MyTag";

    private static final int REQUEST_CODE = 100;

    LinearLayout linlaHeaderProgress;

    String username;

    TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_view);


        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getApplicationContext());
        // user already logged in show dash board
        username = db.getUsername();

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        // Set Tabs inside Toolbar
        tabs = (TabLayout) findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);

        Intent i = getIntent();
        str_username = i.getStringExtra("username");
//        str_profile_picture = i.getStringExtra("profile_pic");
//        str_profile_banner = i.getStringExtra("profile_banner");
//        str_name = i.getStringExtra("name");
//        str_postsct = i.getStringExtra("posts_count");
//        str_status = i.getStringExtra("status");
//        str_followingct = i.getStringExtra("following_count");
//        str_followersct = i.getStringExtra("followers_count");
//        str_profile_verified = i.getStringExtra("verified");

        back = (ImageButton) findViewById(R.id.back);

        followButton = (Button)findViewById(R.id.settings);




        my_profile_followers_count = (TextView) findViewById(R.id.followers_count);
        my_profile_following_count = (TextView) findViewById(R.id.following_count);

        my_profile_profile_pic = (ImageView) findViewById(R.id.profilePic);

        my_profile_name = (TextView) findViewById(R.id.profileName);
        my_profileLocation = (TextView) findViewById(R.id.profileLocation);
        my_profile_profilebio = (TextView) findViewById(R.id.profileBio);
        my_profileLink = (TextView) findViewById(R.id.profileLink);


        rel_following = (RelativeLayout) findViewById(R.id.rel_following);
        rel_followers = (RelativeLayout) findViewById(R.id.rel_followers);


//        my_profileLink.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String url = my_profileLink.getText().toString();
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
//
//
//
//            }
//        });

        message = (ImageButton) findViewById(R.id.message);

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getApplicationContext(),"No Internet Connection", Toast.LENGTH_SHORT).show();


        }
        else
        {

            sendRequest();

        }

//        if(str_username.equals("wardkonnect")){
//
//            followButton.setVisibility(View.GONE);
//        }
//
//        else {
//
//            followButton.setVisibility(View.VISIBLE);
//
//        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ProfileView.this, SingleChat.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("username",str_username);
                in.putExtra("profile_pic",str_profile_picture);
                startActivity(in);
            }
        });


        rel_followers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ProfileView.this, Followers.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("username",str_username);
                startActivity(in);
            }
        });


        rel_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(ProfileView.this, Following.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                in.putExtra("username",str_username);
                startActivity(in);
            }
        });


        followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String followbutton_str = followButton.getText().toString();


                if(followbutton_str.equalsIgnoreCase("FOLLOW")){

                    Follow();

                    String number = my_profile_followers_count.getText().toString();
                    int num = Integer.parseInt(number);
                    int nene = num + 1;
                    String str = String.valueOf(nene);

                    my_profile_followers_count.setText(str);

                }

                else if(followbutton_str.equalsIgnoreCase("UNFOLLOW")){


                    Unfollow();

                    String number = my_profile_followers_count.getText().toString();
                    int num = Integer.parseInt(number);
                    int nene = num - 1;
                    String str = String.valueOf(nene);

                    my_profile_followers_count.setText(str);


                }

                else{

                    Toast.makeText(getApplicationContext(),"An error occured",Toast.LENGTH_SHORT).show();

                }




            }
        });

        checkFollowing();


    }


    private void Follow() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/follow/follow.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(),"Followed",Toast.LENGTH_SHORT).show();

                        sendNotification();

                        followButton.setEnabled(true);
                        followButton.setText("UNFOLLOW");
                        followButton.setBackgroundResource(R.mipmap.followbuttongreen);
//                        mProgressDialog.dismiss();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user", username);
                params.put("follows", str_username);

                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        //Toast.makeText(getApplicationContext(),"Please wait...Adding Comment",Toast.LENGTH_SHORT).show();
//        mProgressDialog = new ProgressDialog(ProfileView.this);
//        // Set progressdialog message
//        mProgressDialog.setMessage("Following");
//        mProgressDialog.setIndeterminate(false);
//        mProgressDialog.setCancelable(false);
//        // Show progressdialog
//        mProgressDialog.show();

        followButton.setEnabled(false);

    }



    private void Unfollow() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/follow/unfollow.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(),"Unfollowed",Toast.LENGTH_SHORT).show();

                        followButton.setEnabled(true);
                        followButton.setText("FOLLOW");
                        followButton.setBackgroundResource(R.mipmap.followbuttonblue);
//                        mProgressDialog.dismiss();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user", username);
                params.put("follows", str_username);

                return params;
            }
        };
        requestQueue.add(stringRequest);
        //Toast.makeText(getApplicationContext(),"Please wait...Adding Comment",Toast.LENGTH_SHORT).show();
//        mProgressDialog = new ProgressDialog(ProfileView.this);
//        // Set progressdialog message
//        mProgressDialog.setMessage("Unfollowing");
//        mProgressDialog.setIndeterminate(false);
//        mProgressDialog.setCancelable(false);
//        // Show progressdialog
//        mProgressDialog.show();

        followButton.setEnabled(false);

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQUEST_CODE) {

            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            if (!cd.isConnectingToInternet()) {

                Toast.makeText(getApplicationContext(),"Failed to update profile",Toast.LENGTH_SHORT).show();


            }
            else
            {

                sendRequest();

            }

        }


    }


    private void sendRequest(){


        String JSON_URL = "http://wardkonnect.xyz/app/profile/get_prof_details.php?username="+str_username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getApplicationContext(), "Internet connection is down", Toast.LENGTH_LONG).show();


                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(this);
        DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(PROFILE);

            JSONObject c = jsonarray.getJSONObject(0);
            // Storing  JSON item in a Variable
            str_id = c.getString("id");
            str_username = c.getString("username");
            str_name = c.getString("name");
            str_status = c.getString("status");
            str_location = c.getString("location");
            str_profession = c.getString("profession");
            str_region = c.getString("region");
            str_url = c.getString("url");
            str_reg_id = c.getString("reg_id");
            str_followersct = c.getString("followers_count");
            str_followingct = c.getString("following_count");
            str_postsct = c.getString("posts_count");
            str_profile_picture = c.getString("profile_picture");
            str_profile_banner = c.getString("profile_banner");
            str_profile_verified = c.getString("verified");





        } catch (JSONException e) {
            e.printStackTrace();
        }

        Picasso.with(this).load(str_profile_picture).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.placeholder).transform(new RoundedCornersTransform())
                .into(my_profile_profile_pic);

//        Picasso.with(this).load(str_profile_banner).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
//                .into(my_profile_profile_banner);

        // postsCount.setText("View All Posts");

        my_profileLocation.setText(str_location+" - "+str_region);
        my_profileLink.setText(str_url);


        my_profile_name.setText(str_name+" ("+str_profession+")");
        my_profile_followers_count.setText(str_followersct);
        my_profile_following_count.setText(str_followingct);
        my_profile_profilebio.setText(AndroidEmoji.ensure(str_status, getApplicationContext()));

//        if(str_profile_verified.equalsIgnoreCase("verified")){
//
//            my_profile_verified.setVisibility(View.VISIBLE);
//
//
//        }
//
//        else {
//
//
//            my_profile_verified.setVisibility(View.GONE);
//
//
//
//        }


    }


    private void checkFollowing() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/follow/isFollowing.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if(str_username.equals("wardkonnect")){

                            followButton.setVisibility(View.GONE);
                        }

                        else {

                        if(response.equalsIgnoreCase("userexists")){

                            followButton.setEnabled(true);
                            followButton.setVisibility(View.VISIBLE);
                            followButton.setText("UNFOLLOW");
                            followButton.setBackgroundResource(R.mipmap.followbuttongreen);

                            Log.e("RESPONSE", response);

                        }

                        else {

                            followButton.setEnabled(true);
                            followButton.setVisibility(View.VISIBLE);
                            followButton.setText("FOLLOW");
                            followButton.setBackgroundResource(R.mipmap.followbuttonblue);

                            Log.e("OutOfBounds", response);

                        }


                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", username);
                params.put("user_involved", str_username);

                return params;
            }
        };
        requestQueue.add(stringRequest);
        //Toast.makeText(getApplicationContext(),"Please wait...Adding Comment",Toast.LENGTH_SHORT).show();



    }


    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {


        Profile.Adapter adapter = new Profile.Adapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentPosts(), "Posts");
       // adapter.addFragment(new FragmentImage(), "Image");
       // adapter.addFragment(new FragmentSaved(), "Saved");
        viewPager.setAdapter(adapter);



    }

//    private void setupTabIcons() {
//        tabs.getTabAt(0).setIcon(tabIcons[0]);
//        tabs.getTabAt(1).setIcon(tabIcons[1]);
//        tabs.getTabAt(2).setIcon(tabIcons[2]);
//        tabs.getTabAt(3).setIcon(tabIcons[3]);
//    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

//            return null;

            return mFragmentTitleList.get(position);

        }



    }



    private void sendNotification() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/notifications/send_ios_notification.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
                UserFunctions  userFunctions = new UserFunctions();
                userFunctions.isUserLoggedIn(getApplicationContext());
                // user already logged in show dash board
                String user = db.getUsername();

                String message = user+" followed you";

                params.put("message", message);
                params.put("regId", str_reg_id);

                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


}