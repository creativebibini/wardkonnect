package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class View_post_notifications extends AppCompatActivity {

    static String POSTS = "posts";
    static String POST_ID = "id";
    static String POST_USERNAME = "username";
    static String POST_NAME = "name";
    static String POST_PROFILE_PIC = "profile_pic";
    static String POST_PROFILE_BANNER = "profile_banner";
    static String POST_FOLLOWERS_CT = "followers_count";
    static String POST_FOLLOWING_CT = "following_count";
    static String POST_POSTS_CT = "posts_count";
    static String POST_PHOTO = "photo";
    static String POST_TEXT = "text";
    static String POST_TIME = "time";
    static String POST_STATUS = "status";
    static String POST_VERIFIED = "verified";
    static String POST_SUSPENDED = "suspended";
    static String POST_COMMENTS_COUNT = "comments_count";
    static String POST_LIKES_COUNT = "likes_count";
    static String POST_LIKED_BY = "liked_by";
    static String POST_REG_ID = "reg_id";
    static String POST_TAG = "tag";


    public static final String TAG = "MyTag";

    SwipeRefreshLayout mSwipeRefreshLayout;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    ListViewAdapter_Notifications_View adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    LinearLayout linlaHeaderProgress;
    ListView lv;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    String username;

    ImageButton back;

    String post_id;

    private static final int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_view_post_notifications);

        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        lv = (ListView) findViewById(R.id.listview_home);

        Intent i = getIntent();
        post_id = i.getStringExtra("post_id");

        back = (ImageButton)findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getApplicationContext());
        // user already logged in show dash board
        username = db.getUsername();


        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {



            ThreadPool.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(getApplicationContext(), "Couldn't refresh", Toast.LENGTH_LONG).show();

                }
            });


        }
        else
        {

            sendRequest();


        }


        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                if (!cd.isConnectingToInternet()) {



                    ThreadPool.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();

                        }
                    });

                    mSwipeRefreshLayout.setRefreshing(false);

                } else {
                    sendRequestRefresh();
                }

            }

        });


    }


    private void sendRequest(){

        String JSON_URL = "http://wardkonnect.xyz/app/posts/get_single_user_post.php?id="+post_id+"&mainuser="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        ThreadPool.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(getApplicationContext(), "Internet connection is down", Toast.LENGTH_LONG).show();

                            }
                        });
                        linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(this);
        DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(POSTS);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("photo", jsonobject.getString("photo"));
                map.put("text", jsonobject.getString("text"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("comments_count", jsonobject.getString("comments_count"));
                map.put("likes_count", jsonobject.getString("likes_count"));
                map.put("liked_by", jsonobject.getString("liked_by"));
                map.put("tag", jsonobject.getString("tag"));
                map.put("reg_id", jsonobject.getString("reg_id"));

                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }




        adapter = new ListViewAdapter_Notifications_View(getApplicationContext(), arraylist, View_post_notifications.this);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);




    }


    public void sendRequestRefresh(){

        String JSON_URL = "http://wardkonnect.xyz/app/posts/get_single_user_post.php?id="+post_id+"&mainuser="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONref(response);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        ThreadPool.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(getApplicationContext(), "Internet connection is down", Toast.LENGTH_LONG).show();

                            }
                        });
                        linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(this);
        DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        mSwipeRefreshLayout.setRefreshing(true);
    }


    private void showJSONref(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(POSTS);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("photo", jsonobject.getString("photo"));
                map.put("text", jsonobject.getString("text"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("comments_count", jsonobject.getString("comments_count"));
                map.put("likes_count", jsonobject.getString("likes_count"));
                map.put("liked_by", jsonobject.getString("liked_by"));
                map.put("tag", jsonobject.getString("tag"));
                map.put("reg_id", jsonobject.getString("reg_id"));

                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }






        adapter = new ListViewAdapter_Notifications_View(getApplicationContext(), arraylist, View_post_notifications.this);
        lv.setAdapter(adapter);


    }

}
