package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    EditText inputUsername;
    EditText inputPassword;
    Button btnLogin;

    TextView forgot_pwd;
    TextView signup_txt;

    RequestQueue requestQueue;
    StringRequest stringRequest;


    // JSON Response node names
    private static String KEY_SUCCESS = "success";
    private static String KEY_ERROR = "error";
    private static String KEY_ERROR_MSG = "error_msg";
    private static String KEY_UID = "uid";
    private static String KEY_USERNAME = "username";
    private static String KEY_NAME = "name";
    private static String KEY_EMAIL = "email";
    private static String KEY_PHONE = "phone";
    private static String KEY_CREATED_AT = "created_at";

    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        setContentView(R.layout.activity_login);

        inputUsername = (EditText) findViewById(R.id.username_et);
        inputPassword = (EditText) findViewById(R.id.password_et);

        btnLogin = (Button) findViewById(R.id.login_btn);

        forgot_pwd = (TextView) findViewById(R.id.forgot_pwd);
        signup_txt = (TextView) findViewById(R.id.signup_txt);
        forgot_pwd = (TextView) findViewById(R.id.forgot_pwd);


        signup_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(Login.this, Signup.class);
                startActivity(in);

            }
        });

        forgot_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(Login.this, PasswordReset.class);
                startActivity(in);

            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {



            public void onClick(View view) {

                //linlaHeaderProgress.setVisibility(View.VISIBLE);

                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                if (!cd.isConnectingToInternet()) {

                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

                    return;
                }
                else
                {


                    String username = inputUsername.getText().toString();
                    String password = inputPassword.getText().toString();



                    if(( inputUsername.getText().toString().equals("")) && ( inputPassword.getText().toString().equals(""))){

                        {
                            // mProgressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Fields can't be blank", Toast.LENGTH_LONG).show();
                            //linlaHeaderProgress.setVisibility(View.GONE);
                           // linlaHeaderProgress.setVisibility(View.GONE);
                        }


                    }

                    else if(( inputUsername.getText().toString().equals(""))){

                        {
                            //	mProgressDialog.dismiss();
                            //Toast.makeText(getApplicationContext(), "suptag field is empty", Toast.LENGTH_LONG).show();
                            inputUsername.setError("Username field is empty");
                            // linlaHeaderProgress.setVisibility(View.GONE);
                           // linlaHeaderProgress.setVisibility(View.GONE);
                        }

                    }




                    else if(( inputPassword.getText().toString().equals(""))){

                        {
                            //	mProgressDialog.dismiss();
                            //Toast.makeText(getApplicationContext(), "password field is empty", Toast.LENGTH_LONG).show();
                            inputPassword.setError("Password field is empty");
                            //linlaHeaderProgress.setVisibility(View.GONE);
                            //mProgressDialog.show();
                           // linlaHeaderProgress.setVisibility(View.GONE);
                        }

                    }





                    else {

                        Toast.makeText(getApplicationContext(),"Please wait",Toast.LENGTH_SHORT).show();
                        //   linlaHeaderProgress.setVisibility(View.VISIBLE);
                        btnLogin.setText("Logging In");



                        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/index.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // response
                                        Log.d("Response", response);
                                        //   Toast.makeText(getApplicationContext(),response,Toast.LENGTH_SHORT).show();


                                        try {
                                            JSONObject jObj = new JSONObject(response);
                                            // boolean error = jObj.getBoolean("error");

                                            String res = jObj.getString(KEY_SUCCESS);

                                            // Check for error node in json
                                            if (Integer.parseInt(res) == 1) {
                                                // user successfully logged in

                                                // Now store the user in SQLite
                                                String uid = jObj.getString("uid");

                                                JSONObject user = jObj.getJSONObject("user");
                                                String name = user.getString("name");
                                                String username = user.getString("username");
                                                String email = user.getString("email");
                                                String phone = user.getString("phone");
                                                String created_at = user.getString("created_at");



                                                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                                                // Inserting row in users table
                                                db.addUser(name, username, email, phone, uid, created_at);

                                                // Launch main activity
                                                Intent intent = new Intent(Login.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                // Error in login. Get the error message
                                                String errorMsg = jObj.getString("error_msg");
                                                Toast.makeText(getApplicationContext(),
                                                        errorMsg, Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            // JSON error
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                        }

                                        btnLogin.setText("LOGIN");

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {


                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("username", inputUsername.getText().toString());
                                params.put("password", inputPassword.getText().toString());
                                params.put("tag", "login");

                                return params;
                            }
                        };

                        requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);
                        //Toast.makeText(getApplicationContext(),"Please wait...Submitting Report",Toast.LENGTH_SHORT).show();
//                        mProgressDialog = new ProgressDialog(getApplicationContext());
//                        // Set progressdialog message
//                        mProgressDialog.setMessage("Loggin In");
//                        mProgressDialog.setIndeterminate(false);
//                        // Show progressdialog
//                        mProgressDialog.show();



                    }
                }
            }
        });



    }
}
