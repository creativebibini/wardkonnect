package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchActivityResults extends AppCompatActivity {

    String searchtxt;

    static String ID = "id";
    static String USERNAME = "username";
    static String POSTS_COUNT = "posts_count";
    static String STATUS = "status";
    static String VERIFIED = "verified";
    static String FOLLOWING_COUNT = "following_count";
    static String FOLLOWERS_COUNT = "followers_count";
    static String PROFILE_PIC = "profile_pic";
    static String PROFILE_BANNER = "profile_banner";

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListViewAdapter_following adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    ListView listview;

    LinearLayout linlaHeaderProgress;

    ImageButton back;

    RequestQueue requestQueue;
    StringRequest stringRequest;
    public static final String TAG = "CommPost";

    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        back = (ImageButton)findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent i = getIntent();
        searchtxt = i.getStringExtra("searchstr");

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getApplicationContext());
        // user already logged in show dash board
        username = db.getUsername();


        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        listview = (ListView)findViewById(R.id.followingLV);

       // Toast.makeText(getApplicationContext(),searchtxt ,Toast.LENGTH_SHORT).show();

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getApplicationContext(),"No Internet Connection", Toast.LENGTH_SHORT).show();

            return;
        }


        else
        {
            sendRequest();
        }

    }

    private void sendRequest(){



        String JSON_URL = "http://wardkonnect.xyz/app/search/get_results.php?user="+username+"&search="+searchtxt;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SearchActivityResults.this,"Internet connection is down", Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("search");

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                // Set the JSON Objects into the array



                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_following(SearchActivityResults.this, arraylist);
        // Set the adapter to the ListView
        listview.setAdapter(adapter);
    }
}
