package wka.com.wardkonnect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class WelcomeScreen extends AppCompatActivity {

    TextView login;
    Button letstart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        login = (TextView) findViewById(R.id.login_txt);
        letstart = (Button) findViewById(R.id.letstart);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(WelcomeScreen.this, Login.class);
                startActivity(in);

            }
        });


        letstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(WelcomeScreen.this, Tips.class);
                startActivity(in);

            }
        });

    }
}
