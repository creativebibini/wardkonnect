package wka.com.wardkonnect;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SinglePostView extends AppCompatActivity {

    LinearLayout linlaHeaderProgress;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    JSONObject jsonobject;
    JSONArray jsonarray;

    ArrayList<HashMap<String, String>> arraylist;

    public static final String TAG = "MyTag";

    String str_id;
    String str_username;
    String str_name;
    String str_profile_pic;
    String str_profile_banner;
    String str_followers_count;
    String str_following_count;
    String str_posts_count;
    String str_photo;
    String str_text;
    String str_time;
    String str_status;
    String str_verified;
    String str_comments_count;
    String str_likes_count;
    String str_liked_by;
    String str_tag;
    String str_reg_id;






    String str_single_id;
    String str_single_comments_count;
    String str_single_likes_count;
    String str_single_liked_by;


    // Declare Variables
    TextView usernameTV;
    TextView timeTV;
    TextView postTV;
    TextView schyrTV;

    TextView likesCount;
    //TextView likestxt;
    TextView commentCount;
    //TextView testimonyTxt;

    SquareImageView postImg;
    ImageView profilePicImg;

    ImageView verifiedImg;

    ImageButton commentBtn;
    ImageButton reportBtn;

//    TextView repostCount;
//
//    ImageButton shareBtn;
//    ImageButton repostBtn;
//    ImageButton more;

    ImageButton likeBtn;

    ImageButton back;

    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post_view);

        back = (ImageButton) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getApplicationContext());
        // user already logged in show dash board
        username = db.getUsername();


        Intent c = getIntent();
        str_id = c.getStringExtra("id");
        str_username = c.getStringExtra("username");
        str_name = c.getStringExtra("name");
        str_profile_pic = c.getStringExtra("profile_pic");
        str_profile_banner = c.getStringExtra("profile_banner");
        str_followers_count = c.getStringExtra("followers_count");
        str_following_count = c.getStringExtra("following_count");
        str_posts_count = c.getStringExtra("posts_count");
        str_photo = c.getStringExtra("photo");
        str_text = c.getStringExtra("text");
        str_time = c.getStringExtra("time");
        str_status = c.getStringExtra("status");
        str_verified = c.getStringExtra("verified");
        str_comments_count = c.getStringExtra("comments_count");
        str_likes_count = c.getStringExtra("likes_count");
        str_liked_by = c.getStringExtra("liked_by");
        str_tag = c.getStringExtra("tag");
        str_reg_id = c.getStringExtra("reg_id");

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getApplicationContext(),"Couldn't refresh", Toast.LENGTH_SHORT).show();


        }
        else
        {
            sendRequestT();

        }






        // Locate the TextViews in listview_item.xml
        usernameTV = (TextView) findViewById(R.id.username);
        timeTV = (TextView) findViewById(R.id.time);
        postTV = (TextView) findViewById(R.id.text);

        likesCount = (TextView) findViewById(R.id.likesCount);
       // repostCount = (TextView) findViewById(R.id.repost_count);
        commentCount = (TextView) findViewById(R.id.commentCount);

        // Locate the ImageView in listview_item.xml
        profilePicImg = (ImageView) findViewById(R.id.profile_pic);
        verifiedImg = (ImageView) findViewById(R.id.verified);
        postImg = (SquareImageView) findViewById(R.id.image);
        // holder.postImg.setVisibility(View.GONE);
        // Locate the ImageView in listview_item.xml
        commentBtn = (ImageButton) findViewById(R.id.comment);
      //  shareBtn= (ImageButton) findViewById(R.id.share);
        likeBtn= (ImageButton) findViewById(R.id.like);
     //   repostBtn= (ImageButton) findViewById(R.id.repost);

    //    more = (ImageButton) findViewById(R.id.more);


        likeBtn.setClickable(false);

        postImg.setVisibility(View.GONE);

       // likestxt.setVisibility(View.GONE);
        likeBtn.setVisibility(View.GONE);
        likesCount.setVisibility(View.GONE);
        commentCount.setVisibility(View.GONE);
        commentBtn.setVisibility(View.GONE);


         if(str_photo.equalsIgnoreCase("http://wardkonnect.xyz/app/posts/")){

            postImg.setVisibility(View.GONE);


        }

        else{
            postImg.setVisibility(View.VISIBLE);
            //   Toast.makeText(context,"Post contains no pic",Toast.LENGTH_SHORT).show();
            Picasso.with(this).load(str_photo).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                    .into(postImg);
        }


        if(str_verified.equals("yes")){

            verifiedImg.setVisibility(View.VISIBLE);

        }

        else {

            verifiedImg.setVisibility(View.GONE);


        }


        Picasso.with(this).load(str_profile_pic).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash).transform(new RoundedCornersTransform())
                .into(profilePicImg);



        // Capture position and set results to the TextViews
        usernameTV.setText(str_username);
        timeTV.setText(str_time);

        //     holder.postTV.setText(resultp.get(Fragment_school.POST_TEXT));

       postTV.setText(AndroidEmoji.ensure(str_text, this));

    //    likesCount.setText(str_likes_count);
    //    commentCount.setText(str_comments_count);







        if(str_verified.toString().equals("verified")){

            verifiedImg.setVisibility(View.VISIBLE);

        }

        else {

            verifiedImg.setVisibility(View.GONE);


        }

        final Intent user_profile = new Intent(getApplicationContext(), ProfileView.class);
        final Intent main_profile = new Intent(getApplicationContext(), Profile.class);

        profilePicImg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                if(str_username.toString().equalsIgnoreCase(username)){

                    main_profile.putExtra("id", str_id);
                    main_profile.putExtra("username", str_username);
                    main_profile.putExtra("name", str_name);
                    main_profile.putExtra("status", str_status);
                    main_profile.putExtra("verified", str_verified);
                    main_profile.putExtra("followers_count", str_followers_count);
                    main_profile.putExtra("following_count", str_following_count);
                    main_profile.putExtra("posts_count", str_posts_count);
                    main_profile.putExtra("profile_pic", str_profile_pic);
                    main_profile.putExtra("profile_banner", str_profile_banner);

                    view.getContext().startActivity(main_profile);

                }

                else{

                    user_profile.putExtra("id", str_id);
                    user_profile.putExtra("username", str_username);
                    user_profile.putExtra("name", str_name);
                    user_profile.putExtra("status", str_status);
                    user_profile.putExtra("verified", str_verified);
                    user_profile.putExtra("followers_count", str_followers_count);
                    user_profile.putExtra("following_count", str_following_count);
                    user_profile.putExtra("posts_count", str_posts_count);
                    user_profile.putExtra("profile_pic", str_profile_pic);
                    user_profile.putExtra("profile_banner", str_profile_banner);


                    view.getContext().startActivity(user_profile);
                }


            }
        });

        usernameTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                if(str_username.toString().equalsIgnoreCase(username)){

                    main_profile.putExtra("id", str_id);
                    main_profile.putExtra("username", str_username);
                    main_profile.putExtra("name", str_name);
                    main_profile.putExtra("status", str_status);
                    main_profile.putExtra("verified", str_verified);
                    main_profile.putExtra("followers_count", str_followers_count);
                    main_profile.putExtra("following_count", str_following_count);
                    main_profile.putExtra("posts_count", str_posts_count);
                    main_profile.putExtra("profile_pic", str_profile_pic);
                    main_profile.putExtra("profile_banner", str_profile_banner);

                    view.getContext().startActivity(main_profile);

                }

                else{



                    user_profile.putExtra("id", str_id);
                    user_profile.putExtra("username", str_username);
                    user_profile.putExtra("name", str_name);
                    user_profile.putExtra("status", str_status);
                    user_profile.putExtra("verified", str_verified);
                    user_profile.putExtra("followers_count", str_followers_count);
                    user_profile.putExtra("following_count", str_following_count);
                    user_profile.putExtra("posts_count", str_posts_count);
                    user_profile.putExtra("profile_pic", str_profile_pic);
                    user_profile.putExtra("profile_banner", str_profile_banner);


                    view.getContext().startActivity(user_profile);
                }


            }
        });

        likeBtn.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {



                    if (likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("unliked")) {

                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                            new LikePost().execute();
                            String number = likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num + 1;
                            String str = String.valueOf(nene);

                            likesCount.setText(str);

                            likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp1);
                            likeBtn.setTag(R.string.like_tag,"liked");

                        }

                    } else if (likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("liked")) {


                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                            new UnLikePost().execute();
                            String number = likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num - 1;
                            String str = String.valueOf(nene);

                            likesCount.setText(str);

                            likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
                            likeBtn.setTag(R.string.like_tag,"unliked");

                        }
                    }

                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    if (likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("unliked")) {


                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                           new LikePost().execute();
                            String number = likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num + 1;
                            String str = String.valueOf(nene);

                            likesCount.setText(str);

                            likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp1);
                            likeBtn.setTag(R.string.like_tag,"liked");

                        }

                    } else if (likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("liked")) {


                        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                           new UnLikePost().execute();
                            String number = likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num - 1;
                            String str = String.valueOf(nene);

                            likesCount.setText(str);

                            likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
                            likeBtn.setTag(R.string.like_tag,"unliked");

                        }
                    }

                }


            }
        });

        final Intent post_comment = new Intent(getApplicationContext(), Comments.class);
        commentBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                post_comment.putExtra("id",str_id);
                post_comment.putExtra("username",str_username);
                post_comment.putExtra("reg_id",str_reg_id);


                view.getContext().startActivity(post_comment);



            }
        });

        commentCount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                post_comment.putExtra("id",str_id);
                post_comment.putExtra("username",str_username);
                post_comment.putExtra("reg_id",str_reg_id);

                post_comment.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(post_comment);


            }
        });

        final Intent likes = new Intent(getApplicationContext(), Likes.class);
        likesCount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                likes.putExtra("id",str_id);
                likes.putExtra("username", str_username);

                likes.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                view.getContext().startActivity(likes);



            }
        });


//        likestxt.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                likes.putExtra("id",str_id);
//                likes.putExtra("username", str_username);
//
//                likes.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                view.getContext().startActivity(likes);
//
//
//
//            }
//        });


        final Intent post_report = new Intent(getApplicationContext(), Report.class);
//        reportBtn.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                post_report.putExtra("id",str_id);
//                post_report.putExtra("username",str_username);
//
//
//                view.getContext().startActivity(post_report);
//
//
//
//
//
//            }
//        });


   }


    private void sendRequestT(){


        String JSON_URL = "http://wardkonnect.xyz/app/posts/get_home_single_posts.php?username="+username+"&id="+str_id;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONt(response);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getApplicationContext(), "Internet connection is down", Toast.LENGTH_LONG).show();


                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        DiskBasedCache cache = new DiskBasedCache(getApplicationContext().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



    }


    private void showJSONt(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("posts");

            JSONObject c = jsonarray.getJSONObject(0);
            // Storing  JSON item in a Variable
            str_single_id = c.getString("id");
            str_single_comments_count = c.getString("comments_count");
            str_single_likes_count = c.getString("likes_count");
            str_single_liked_by = c.getString("liked_by");




        } catch (JSONException e) {
            e.printStackTrace();
        }


        if(str_single_liked_by.toString().equals(username)){

            likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp1);
            likeBtn.setTag(R.string.like_tag,"liked");

        }

        else if(!str_single_liked_by.toString().equals(username)){

            likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
            likeBtn.setTag(R.string.like_tag,"unliked");

        }

        else {

            Toast.makeText(getApplicationContext(),"Error Occured", Toast.LENGTH_SHORT).show();
        }



        likesCount.setText(str_single_likes_count);
        commentCount.setText(str_single_comments_count);


//        likestxt.setVisibility(View.VISIBLE);
        likeBtn.setVisibility(View.VISIBLE);
        likesCount.setVisibility(View.VISIBLE);
        commentCount.setVisibility(View.VISIBLE);
        commentBtn.setVisibility(View.VISIBLE);


    }





    public class LikePost extends AsyncTask<String, String, String> {

        boolean hasUserLiked = false;

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://wardkonnect.xyz/app/likes/home_like_post.php");


            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("id", "01"));
                nameValuePairs.add(new BasicNameValuePair("post_id", str_id));
                nameValuePairs.add(new BasicNameValuePair("username", username));
                nameValuePairs.add(new BasicNameValuePair("user_involved", str_username));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpclient.execute(httppost);

                hasUserLiked = true;

            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String lenghtOfFile) {

            //Toast.makeText(getApplicationContext(),"Liked",Toast.LENGTH_SHORT).show();

            runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    if(!str_username.toString().equalsIgnoreCase(username)){

                       //   new SendLikeNotification().execute();
                        //	Toast.makeText(getApplicationContext(),"Liked",Toast.LENGTH_SHORT).show();
                    }


                    else{

                        //	Toast.makeText(getApplicationContext(),"Liked",Toast.LENGTH_SHORT).show();
                    }



                }
            });


        }

    }

    public class UnLikePost extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... strings) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://wardkonnect.xyz/app/likes/home_unlike_post.php");


            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("id", "01"));
                nameValuePairs.add(new BasicNameValuePair("post_id", str_id));
                nameValuePairs.add(new BasicNameValuePair("username", username));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpclient.execute(httppost);



            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String lenghtOfFile) {

        }


    }


    public class SendLikeNotification extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected String doInBackground(String... strings) {

            DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
            UserFunctions userFunctions = new UserFunctions();
            userFunctions.isUserLoggedIn(getApplicationContext());
            // user already logged in show dash board
            String user = db.getUsername();

            String message = user+"%20liked%20your%20post";


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://wardkonnect.xyz/app/firebase/index.php?title=TrueBelievers&message="+message+"&push_type=individual&regId="+str_reg_id);


            try {

                httpclient.execute(httppost);


            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String lenghtOfFile) {

        }


    }

    @Override
    public void onResume() {
        // fetch updated data
        sendRequestT();

        super.onResume();
    }


}
