package wka.com.wardkonnect;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListViewAdapter_Notifications_View extends BaseAdapter {


    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    ImageLoader imageLoader;
    HashMap<String, String> resultp = new HashMap<String, String>();

    private FragmentHome fragment;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    private View_post_notifications pst;

    private static final int REQUEST_CODE = 100;


    public ListViewAdapter_Notifications_View(Context context,ArrayList<HashMap<String, String>> arraylist,  View_post_notifications post) {
        this.context = context;
        this.pst = post;
        data = arraylist;

    }



    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public void notifyDataSetChanged(){

        super.notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolder {

        // Declare Variables
        TextView usernameTV;
        TextView timeTV;
        TextView postTV;

        TextView likesCount;
        TextView likestext;
        TextView commentCount;
        TextView repostCount;

        SquareImageView postImg;
        ImageView profilePicImg;

        ImageView verifiedImg;

        ImageButton commentBtn;
        //ImageButton reportBtn;
        ImageButton shareBtn;
        ImageButton repostBtn;
        ImageButton more;
        ImageButton likeBtn;
    }

    ViewHolder holder;
    String username;

    String myyear;

    String reg_id;



    public View getView(final int position, View itemView, ViewGroup parent) {
        itemView = null;

        if (itemView == null) {


            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            itemView = inflater.inflate(R.layout.list_item_home, parent, false);
            // Get the position
            resultp = data.get(position);
            holder = new ViewHolder();

            // Locate the TextViews in listview_item.xml
            holder.usernameTV = (TextView) itemView.findViewById(R.id.username);
            holder.timeTV = (TextView) itemView.findViewById(R.id.time);
            holder.postTV = (TextView) itemView.findViewById(R.id.text);

            holder.likesCount = (TextView) itemView.findViewById(R.id.likesCount);
            holder.likestext = (TextView) itemView.findViewById(R.id.likestext);
            holder.repostCount = (TextView) itemView.findViewById(R.id.repost_count);
            holder.commentCount = (TextView) itemView.findViewById(R.id.commentCount);

            // Locate the ImageView in listview_item.xml
            holder.profilePicImg = (ImageView) itemView.findViewById(R.id.profile_pic);
            holder.verifiedImg = (ImageView) itemView.findViewById(R.id.verified);
            holder.postImg = (SquareImageView) itemView.findViewById(R.id.image);
            // holder.postImg.setVisibility(View.GONE);
            // Locate the ImageView in listview_item.xml
            holder.commentBtn = (ImageButton) itemView.findViewById(R.id.comment);
            holder.shareBtn= (ImageButton) itemView.findViewById(R.id.share);
            holder.likeBtn= (ImageButton) itemView.findViewById(R.id.like);
            holder.repostBtn= (ImageButton) itemView.findViewById(R.id.repost);

            holder.more= (ImageButton) itemView.findViewById(R.id.more);

            holder.likeBtn.setClickable(false);

            holder.postImg.setVisibility(View.GONE);

        }

        else{

            holder = (ViewHolder) itemView.getTag();
        }

        reg_id = resultp.get(FragmentHome.POST_REG_ID);



        String text = resultp.get(FragmentHome.POST_TEXT).toString();

        if(text.equals("")){

            holder.postTV.setVisibility(View.GONE);

        }

        else {

            holder.postTV.setVisibility(View.VISIBLE);
            holder.postTV.setText(AndroidEmoji.ensure(resultp.get(FragmentHome.POST_TEXT), context));

        }

        //       String textitxt = resultp.get(FragmentHome.POST_TAG).toString();

//        if(textitxt.equals("")){
//
//            holder.testimonyTxt.setVisibility(View.GONE);
//
//        }
//
//        else {
//
//            holder.testimonyTxt.setVisibility(View.VISIBLE);
//
//        }


        String photo = resultp.get(FragmentHome.POST_PHOTO).toString();

        if(photo.equalsIgnoreCase("http://wardkonnect.xyz/app/posts/")){

            holder.postImg.setVisibility(View.GONE);


        }

        else{
            holder.postImg.setVisibility(View.VISIBLE);
            //   Toast.makeText(context,"Post contains no pic",Toast.LENGTH_SHORT).show();
            Picasso.with(context).load(resultp.get(FragmentHome.POST_PHOTO)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                    .into(holder.postImg);
        }



        Picasso.with(context).load(resultp.get(FragmentHome.POST_PROFILE_PIC)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash).transform(new RoundedCornersTransform())
                .into(holder.profilePicImg);


        //  Picasso.with(context).load(resultp.get(FragmentHome.POST_PROFILE_PIC)).transform(new RoundedCornersTransformation(10,10)).resize(175,300).into(holder.profilePicImg);

        // Capture position and set results to the TextViews
        holder.usernameTV.setText(resultp.get(FragmentHome.POST_USERNAME));
        holder.timeTV.setText(resultp.get(FragmentHome.POST_TIME));

        //     holder.postTV.setText(resultp.get(Fragment_school.POST_TEXT));

        //   holder.postTV.setText(AndroidEmoji.ensure(resultp.get(FragmentHome.POST_TEXT), context));

        holder.likesCount.setText(resultp.get(FragmentHome.POST_LIKES_COUNT));
        holder.commentCount.setText(resultp.get(FragmentHome.POST_COMMENTS_COUNT));

        //      holder.schyrTV.setText(schyr);


        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();

        if(resultp.get(FragmentHome.POST_LIKED_BY).toString().equals(username)){

            holder.likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp1);
            holder.likeBtn.setTag(R.string.like_tag,"liked");

        }

        else if(!resultp.get(FragmentHome.POST_LIKED_BY).toString().equals(username)){

            holder.likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
            holder.likeBtn.setTag(R.string.like_tag,"unliked");

        }

        else {

            Toast.makeText(context,"Error Occured",Toast.LENGTH_SHORT).show();
        }



        if(resultp.get(FragmentHome.POST_VERIFIED).toString().equals("yes")){

            holder.verifiedImg.setVisibility(View.VISIBLE);

        }

        else {

            holder.verifiedImg.setVisibility(View.GONE);


        }


        holder.more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                PopupMenu popup = new PopupMenu(context, view);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());



                if(resultp.get(FragmentHome.POST_USERNAME).toString().equals(username)){

                    popup.getMenu().findItem(R.id.viewprofile).setVisible(true);
                    popup.getMenu().findItem(R.id.share).setVisible(true);
                    popup.getMenu().findItem(R.id.delete).setVisible(true);
                    popup.getMenu().findItem(R.id.report).setVisible(false);

                }


                else if(resultp.get(FragmentHome.POST_USERNAME).toString().equals("wardkonnect")){

                    popup.getMenu().findItem(R.id.viewprofile).setVisible(true);
                    popup.getMenu().findItem(R.id.share).setVisible(true);
                    popup.getMenu().findItem(R.id.delete).setVisible(false);
                    popup.getMenu().findItem(R.id.report).setVisible(false);

                }

                else {

                    popup.getMenu().findItem(R.id.viewprofile).setVisible(true);
                    popup.getMenu().findItem(R.id.share).setVisible(true);
                    popup.getMenu().findItem(R.id.delete).setVisible(false);
                    popup.getMenu().findItem(R.id.report).setVisible(true);


                }


                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        // Toast.makeText(context, "You Clicked : " + item.getTitle(), Toast.LENGTH_SHORT).show();



                        final Intent user_profile = new Intent(context, ProfileView.class);
                        final Intent main_profile = new Intent(context, Profile.class);

                        switch (item.getItemId()) {



                            case R.id.viewprofile:
                                if(resultp.get(FragmentHome.POST_USERNAME).toString().equalsIgnoreCase(username)){
                                    main_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    main_profile.putExtra("id", resultp.get(FragmentHome.POST_ID));
                                    main_profile.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));
                                    main_profile.putExtra("name", resultp.get(FragmentHome.POST_NAME));
                                    main_profile.putExtra("status", resultp.get(FragmentHome.POST_STATUS));
                                    main_profile.putExtra("verified", resultp.get(FragmentHome.POST_VERIFIED));
                                    main_profile.putExtra("followers_count", resultp.get(FragmentHome.POST_FOLLOWERS_CT));
                                    main_profile.putExtra("following_count", resultp.get(FragmentHome.POST_FOLLOWING_CT));
                                    main_profile.putExtra("posts_count", resultp.get(FragmentHome.POST_POSTS_CT));
                                    main_profile.putExtra("profile_pic", resultp.get(FragmentHome.POST_PROFILE_PIC));
                                    main_profile.putExtra("profile_banner", resultp.get(FragmentHome.POST_PROFILE_BANNER));

                                    context.startActivity(main_profile);

                                }

                                else{
                                    user_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    user_profile.putExtra("id", resultp.get(FragmentHome.POST_ID));
                                    user_profile.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));
                                    user_profile.putExtra("name", resultp.get(FragmentHome.POST_NAME));
                                    user_profile.putExtra("status", resultp.get(FragmentHome.POST_STATUS));
                                    user_profile.putExtra("verified", resultp.get(FragmentHome.POST_VERIFIED));
                                    user_profile.putExtra("followers_count", resultp.get(FragmentHome.POST_FOLLOWERS_CT));
                                    user_profile.putExtra("following_count", resultp.get(FragmentHome.POST_FOLLOWING_CT));
                                    user_profile.putExtra("posts_count", resultp.get(FragmentHome.POST_POSTS_CT));
                                    user_profile.putExtra("profile_pic", resultp.get(FragmentHome.POST_PROFILE_PIC));
                                    user_profile.putExtra("profile_banner", resultp.get(FragmentHome.POST_PROFILE_BANNER));

                                    context.startActivity(user_profile);
                                }
                                break;

                            case R.id.report:


                                Intent report = new Intent(context, Report.class);
                                report.putExtra("id",resultp.get(FragmentHome.POST_ID));
                                context.startActivity(report);

                                break;


                            case R.id.share:

                                String photo = resultp.get(FragmentHome.POST_PHOTO).toString();


                                final String name_ct = resultp.get(FragmentHome.POST_NAME).toString();
                                final String ct = resultp.get(FragmentHome.POST_TEXT).toString();
                                final String headline = name_ct+"`s Post on WardKonnect";
                                final String new_content = ct.substring(0, Math.min(ct.length(), 700));



                                if(photo.equals("http://wardkonnect.xyz/app/posts/")){

                                    Intent i = new Intent(Intent.ACTION_SEND);
                                    i.setType("text/plain");
                                    i.putExtra(Intent.EXTRA_TEXT, headline+"\n\n"+new_content+"..."+"\n\n"+"Get more at https://play.google.com/store/apps/details?id=wk.com.wardkonnect");
                                    context.startActivity(Intent.createChooser(i, "Share "));

                                }


                                else {
                                    Picasso.with(context).load(photo).into(new Target() {
                                        @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                            Intent i = new Intent(Intent.ACTION_SEND);
                                            i.setType("image/*");
                                            i.putExtra(Intent.EXTRA_TEXT, headline+"\n\n"+new_content+"..."+"\n\n"+"Get more at https://play.google.com/store/apps/details?id=wk.com.wardkonnect");
                                            i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                                            context.startActivity(Intent.createChooser(i, "Share Image"));
                                        }
                                        @Override public void onBitmapFailed(Drawable errorDrawable) { }
                                        @Override public void onPrepareLoad(Drawable placeHolderDrawable) { }
                                    });


                                }



                                break;


                            case R.id.delete:



                                stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/posts/delete-post.php",
                                        new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                // response
                                                Log.d("Response", response);
                                                //          mProgressDialog.dismiss();]

                                                ((FragmentHome) fragment).sendRequestRefresh();

                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {


                                            }
                                        }
                                ) {
                                    @Override
                                    protected Map<String, String> getParams() {
                                        Map<String, String> params = new HashMap<String, String>();

                                        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
                                        UserFunctions  userFunctions = new UserFunctions();
                                        userFunctions.isUserLoggedIn(context);
                                        // user already logged in show dash board
                                        String user = db.getUsername();



                                        params.put("id", resultp.get(FragmentHome.POST_ID));
                                        params.put("username", user);

                                        return params;
                                    }
                                };

                                requestQueue = Volley.newRequestQueue(context);
                                requestQueue.add(stringRequest);






                                break;
                        }

                        return true;
                    }
                });

                popup.show(); //showing popup menu
            }
        });


        final Intent user_profile = new Intent(context, ProfileView.class);
        final Intent main_profile = new Intent(context, Profile.class);

        holder.profilePicImg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resultp = data.get(position);

                if(resultp.get(FragmentHome.POST_USERNAME).toString().equalsIgnoreCase(username)){
                    main_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    main_profile.putExtra("id", resultp.get(FragmentHome.POST_ID));
                    main_profile.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));
                    main_profile.putExtra("name", resultp.get(FragmentHome.POST_NAME));
                    main_profile.putExtra("status", resultp.get(FragmentHome.POST_STATUS));
                    main_profile.putExtra("verified", resultp.get(FragmentHome.POST_VERIFIED));
                    main_profile.putExtra("followers_count", resultp.get(FragmentHome.POST_FOLLOWERS_CT));
                    main_profile.putExtra("following_count", resultp.get(FragmentHome.POST_FOLLOWING_CT));
                    main_profile.putExtra("posts_count", resultp.get(FragmentHome.POST_POSTS_CT));
                    main_profile.putExtra("profile_pic", resultp.get(FragmentHome.POST_PROFILE_PIC));
                    main_profile.putExtra("profile_banner", resultp.get(FragmentHome.POST_PROFILE_BANNER));

                    context.startActivity(main_profile);

                }

                else{
                    user_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    user_profile.putExtra("id", resultp.get(FragmentHome.POST_ID));
                    user_profile.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));
                    user_profile.putExtra("name", resultp.get(FragmentHome.POST_NAME));
                    user_profile.putExtra("status", resultp.get(FragmentHome.POST_STATUS));
                    user_profile.putExtra("verified", resultp.get(FragmentHome.POST_VERIFIED));
                    user_profile.putExtra("followers_count", resultp.get(FragmentHome.POST_FOLLOWERS_CT));
                    user_profile.putExtra("following_count", resultp.get(FragmentHome.POST_FOLLOWING_CT));
                    user_profile.putExtra("posts_count", resultp.get(FragmentHome.POST_POSTS_CT));
                    user_profile.putExtra("profile_pic", resultp.get(FragmentHome.POST_PROFILE_PIC));
                    user_profile.putExtra("profile_banner", resultp.get(FragmentHome.POST_PROFILE_BANNER));

                    context.startActivity(user_profile);
                }


            }
        });

        holder.usernameTV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resultp = data.get(position);

                if(resultp.get(FragmentHome.POST_USERNAME).toString().equalsIgnoreCase(username)){
                    main_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    main_profile.putExtra("id", resultp.get(FragmentHome.POST_ID));
                    main_profile.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));
                    main_profile.putExtra("name", resultp.get(FragmentHome.POST_NAME));
                    main_profile.putExtra("status", resultp.get(FragmentHome.POST_STATUS));
                    main_profile.putExtra("verified", resultp.get(FragmentHome.POST_VERIFIED));
                    main_profile.putExtra("followers_count", resultp.get(FragmentHome.POST_FOLLOWERS_CT));
                    main_profile.putExtra("following_count", resultp.get(FragmentHome.POST_FOLLOWING_CT));
                    main_profile.putExtra("posts_count", resultp.get(FragmentHome.POST_POSTS_CT));
                    main_profile.putExtra("profile_pic", resultp.get(FragmentHome.POST_PROFILE_PIC));
                    main_profile.putExtra("profile_banner", resultp.get(FragmentHome.POST_PROFILE_BANNER));

                    context.startActivity(main_profile);

                }

                else{
                    user_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    user_profile.putExtra("id", resultp.get(FragmentHome.POST_ID));
                    user_profile.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));
                    user_profile.putExtra("name", resultp.get(FragmentHome.POST_NAME));
                    user_profile.putExtra("status", resultp.get(FragmentHome.POST_STATUS));
                    user_profile.putExtra("verified", resultp.get(FragmentHome.POST_VERIFIED));
                    user_profile.putExtra("followers_count", resultp.get(FragmentHome.POST_FOLLOWERS_CT));
                    user_profile.putExtra("following_count", resultp.get(FragmentHome.POST_FOLLOWING_CT));
                    user_profile.putExtra("posts_count", resultp.get(FragmentHome.POST_POSTS_CT));
                    user_profile.putExtra("profile_pic", resultp.get(FragmentHome.POST_PROFILE_PIC));
                    user_profile.putExtra("profile_banner", resultp.get(FragmentHome.POST_PROFILE_BANNER));


                    context.startActivity(user_profile);
                }


            }
        });


        holder.likeBtn.setTag(holder);
        holder.likeBtn.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {




                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

                    holder = (ViewHolder) view.getTag();
                    resultp = data.get(position);

                    if (holder.likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("unliked")) {
                        resultp = data.get(position);

                        ConnectionDetector cd = new ConnectionDetector(context);
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                            new LikePost().execute();
                            String number = holder.likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num + 1;
                            String str = String.valueOf(nene);

                            holder.likesCount.setText(str);

                            holder.likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp1);
                            holder.likeBtn.setTag(R.string.like_tag, "liked");

                        }

                    } else if (holder.likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("liked")) {
                        resultp = data.get(position);

                        ConnectionDetector cd = new ConnectionDetector(context);
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                            new UnLikePost().execute();
                            String number = holder.likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num - 1;
                            String str = String.valueOf(nene);

                            holder.likesCount.setText(str);

                            holder.likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
                            holder.likeBtn.setTag(R.string.like_tag, "unliked");

                        }
                    }

                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                    holder = (ViewHolder) view.getTag();
                    resultp = data.get(position);

                    if (holder.likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("unliked")) {
                        resultp = data.get(position);

                        ConnectionDetector cd = new ConnectionDetector(context);
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                            new LikePost().execute();
                            String number = holder.likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num + 1;
                            String str = String.valueOf(nene);

                            holder.likesCount.setText(str);

                            holder.likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp1);
                            holder.likeBtn.setTag(R.string.like_tag, "liked");

                        }

                    } else if (holder.likeBtn.getTag(R.string.like_tag).toString().equalsIgnoreCase("liked")) {
                        resultp = data.get(position);

                        ConnectionDetector cd = new ConnectionDetector(context);
                        if (!cd.isConnectingToInternet()) {

                            Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show();


                        } else {

                            new UnLikePost().execute();
                            String number = holder.likesCount.getText().toString();
                            int num = Integer.parseInt(number);
                            int nene = num - 1;
                            String str = String.valueOf(nene);

                            holder.likesCount.setText(str);

                            holder.likeBtn.setImageResource(R.drawable.ic_favorite_black_24dp);
                            holder.likeBtn.setTag(R.string.like_tag, "unliked");

                        }
                    }

                }


            }


        });




        final Intent post_comment = new Intent(context, Comments.class);
        holder.commentBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                post_comment.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                post_comment.putExtra("id",resultp.get(FragmentHome.POST_ID));
                post_comment.putExtra("username",resultp.get(FragmentHome.POST_USERNAME));
                post_comment.putExtra("suspended",resultp.get(FragmentHome.POST_SUSPENDED));
                post_comment.putExtra("reg_id",resultp.get(FragmentHome.POST_REG_ID));
                context.startActivity(post_comment);

                //       notifyDataSetChanged();


            }
        });

        holder.commentCount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                post_comment.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                post_comment.putExtra("id", resultp.get(FragmentHome.POST_ID));
                post_comment.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));
                post_comment.putExtra("suspended",resultp.get(FragmentHome.POST_SUSPENDED));
                post_comment.putExtra("reg_id",resultp.get(FragmentHome.POST_REG_ID));

                context.startActivity(post_comment);

                //     notifyDataSetChanged();
            }
        });

        final Intent likes = new Intent(context, Likes.class);
        holder.likesCount.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                likes.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                likes.putExtra("id",resultp.get(FragmentHome.POST_ID));
                likes.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));

                context.startActivity(likes);

                //notifyDataSetChanged();

            }
        });


        holder.likestext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                likes.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                likes.putExtra("id",resultp.get(FragmentHome.POST_ID));
                likes.putExtra("username", resultp.get(FragmentHome.POST_USERNAME));

                context.startActivity(likes);



            }
        });


//        final Intent post_report = new Intent(context, Report.class);
//        holder.reportBtn.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//                resultp = data.get(position);
//                post_report.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                post_report.putExtra("id",resultp.get(FragmentHome.POST_ID));
//                post_report.putExtra("username",resultp.get(FragmentHome.POST_USERNAME));
//                context.startActivity(post_report);
//
//
//
//
//
//
//            }
//        });

        return itemView;
    }

    public class LikePost extends AsyncTask<String, String, String> {

        boolean hasUserLiked = false;

        protected void onPreExecute() {
            super.onPreExecute();

            //    notifyDataSetChanged();

        }

        @Override
        protected String doInBackground(String... strings) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://wardkonnect.xyz/app/likes/home_like_post.php");


            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("id", "01"));
                nameValuePairs.add(new BasicNameValuePair("post_id", resultp.get(FragmentHome.POST_ID)));
                nameValuePairs.add(new BasicNameValuePair("username", username));
                nameValuePairs.add(new BasicNameValuePair("user_involved", resultp.get(FragmentHome.POST_USERNAME)));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpclient.execute(httppost);

                hasUserLiked = true;

            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String lenghtOfFile) {

            //Toast.makeText(getApplicationContext(),"Liked",Toast.LENGTH_SHORT).show();

//            context.runOnUiThread(new Runnable() {
//
//                @Override
//                public void run() {
//
            if(!resultp.get(FragmentHome.POST_USERNAME).toString().equalsIgnoreCase(username)){

                sendNotification();
            }
//
//
            else{
//
//                        //	Toast.makeText(getApplicationContext(),"Liked",Toast.LENGTH_SHORT).show();
            }
//
//
//
//                }
            //          });

            //           notifyDataSetChanged();

//            FragmentHome fh = new FragmentHome();
//            fh.sendFeedREfresh();

        //    ((FragmentHome) fragment).sendFeedREfresh();


        }

    }

    public class UnLikePost extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();

            //   notifyDataSetChanged();

        }

        @Override
        protected String doInBackground(String... strings) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://wardkonnect.xyz/app/likes/home_unlike_post.php");


            try {
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                nameValuePairs.add(new BasicNameValuePair("id", "01"));
                nameValuePairs.add(new BasicNameValuePair("post_id", resultp.get(FragmentHome.POST_ID)));
                nameValuePairs.add(new BasicNameValuePair("username", username));

                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                httpclient.execute(httppost);



            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String lenghtOfFile) {

            //           FragmentHome fh = new FragmentHome();
            //           fh.sendFeedREfresh();

           // ((FragmentHome) fragment).sendFeedREfresh();

        }


    }

//    public class SendLikeNotification extends AsyncTask<String, String, String> {
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
//            UserFunctions  userFunctions = new UserFunctions();
//            userFunctions.isUserLoggedIn(context);
//            // user already logged in show dash board
//            String user = db.getUsername();
//
//            String message = user+"%20liked%20your%20post";
//
//
//            //String message = "Pdee";
//
//            HttpClient httpclient = new DefaultHttpClient();
//            // HttpPost httppost = new HttpPost("http://truebelieversworld.com/tb/notifications/send_message.php?regId="+reg_id+"&message="+message);
//            HttpPost httppost = new HttpPost("http://www.truebelieversworld.com/tb/firebase/index.php?title=TrueBelievers&message="+message+"&push_type=individual&regId="+reg_id);
//
//
//            try {
//
//                httpclient.execute(httppost);
//
//
//            } catch (NullPointerException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String lenghtOfFile) {
//
//        }
//
//    }


    private void sendNotification() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/notifications/send_ios_notification.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
                UserFunctions  userFunctions = new UserFunctions();
                userFunctions.isUserLoggedIn(context);
                // user already logged in show dash board
                String user = db.getUsername();

                String message = user+" liked your post";

                params.put("message", message);
                params.put("regId", resultp.get(FragmentHome.POST_REG_ID));

                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);

    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public void shareItem(String url) {

    }


}
