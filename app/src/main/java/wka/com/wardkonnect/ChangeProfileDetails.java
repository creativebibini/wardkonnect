package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.StringCharacterIterator;
import java.util.HashMap;
import java.util.Map;

public class ChangeProfileDetails extends AppCompatActivity {

    ImageButton back;

    EditText etBio;
    EditText etLocation;
    EditText etUrl;

    Button btnUpBio;

    RequestQueue requestQueue;

    ProgressDialog mProgressDialog;



    String getStatus;
    String getLocation;
    String getUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_profile_details);

        back = (ImageButton) findViewById(R.id.back);

        Intent sta = getIntent();
        getStatus = sta.getStringExtra("status");
        getLocation = sta.getStringExtra("location");
        getUrl = sta.getStringExtra("url");


        etBio = (EditText)findViewById(R.id.bio_et);
        etLocation = (EditText)findViewById(R.id.location_et);
        etUrl = (EditText)findViewById(R.id.website_et);

        btnUpBio =(Button)findViewById(R.id.update_btn);
        btnUpBio.setClickable(true);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        btnUpBio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(etBio.length() < 3){



                    Toast.makeText(getApplicationContext(),"Bio is too short",Toast.LENGTH_SHORT).show();

                }

                else{

                    updateBio();




                }



            }
        });

        etBio.setText(getStatus);
        etLocation.setText(getLocation);
        etUrl.setText(getUrl);


    }


    public void updateBio(){

        String url = "http://wardkonnect.xyz/app/profile/profile_bio_update.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        mProgressDialog.dismiss();

                        etBio.setText("");
                        btnUpBio.setEnabled(true);


                        Toast.makeText(getApplicationContext(),"Updated", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },

                new Response.ErrorListener()
                {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
                UserFunctions  userFunctions = new UserFunctions();
                userFunctions.isUserLoggedIn(getApplicationContext());
                // user already logged in show dash board
                String username = db.getUsername();


                String biography = etBio.getText().toString();
                String location = etLocation.getText().toString();
                String url = etUrl.getText().toString();

                String message_raw = StringEscapeUtils.escapeJava(biography);
                String message = forJSON(message_raw);


                Map<String, String> params = new HashMap<String, String>();
                params.put("location", location);
                params.put("status", message);
                params.put("link", url);


                params.put("username", username);


                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);

        btnUpBio.setEnabled(false);
        mProgressDialog = new ProgressDialog(this);
        // Set progressdialog message
        mProgressDialog.setMessage("Updating...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();

    }

    public static String forJSON(String aText){
        final StringBuilder result = new StringBuilder();
        StringCharacterIterator iterator = new StringCharacterIterator(aText);
        char character = iterator.current();
        while (character != StringCharacterIterator.DONE){
            if( character == '\"' ){
                result.append("\\\"");
            }
            else if(character == '\\'){
                result.append("\\\\");
            }
            else if(character == '/'){
                result.append("\\/");
            }
            else if(character == '\b'){
                result.append("\\b");
            }
            else if(character == '\f'){
                result.append("\\f");
            }
            else if(character == '\n'){
                result.append("\\n");
            }
            else if(character == '\r'){
                result.append("\\r");
            }
            else if(character == '\t'){
                result.append("\\t");
            }
            else {
                //the char is not a special one
                //add it to the result as is
                result.append(character);
            }
            character = iterator.next();
        }
        return result.toString();
    }
}
