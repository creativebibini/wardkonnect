package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.commons.lang3.StringEscapeUtils;

import java.util.HashMap;
import java.util.Map;

public class SelectedProfession extends AppCompatActivity {

    EditText professionET;
    Button updateBtn;

    ImageButton back;

    String name;

    RequestQueue requestQueue;

    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_profession);

    Intent i = getIntent();
    name = i.getStringExtra("name");

        updateBtn = (Button) findViewById(R.id.updateBtn);
        professionET = (EditText) findViewById(R.id.professionET);

        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        professionET.setText(name);
        professionET.setEnabled(false);

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateBio();


            }
        });




    }


    public void updateBio(){

        String url = "http://wardkonnect.xyz/app/profile/profile_profession_update.php";
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        mProgressDialog.dismiss();



                        Toast.makeText(getApplicationContext(),"Updated", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                },

                new Response.ErrorListener()
                {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
                UserFunctions  userFunctions = new UserFunctions();
                userFunctions.isUserLoggedIn(getApplicationContext());
                // user already logged in show dash board
                String username = db.getUsername();


                Map<String, String> params = new HashMap<String, String>();
                params.put("profession", name);
                params.put("username", username);


                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(postRequest);


        mProgressDialog = new ProgressDialog(this);
        // Set progressdialog message
        mProgressDialog.setMessage("Updating...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();

    }
}
