package wka.com.wardkonnect;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "login_wk";

	// Login table name
	private static final String TABLE_LOGIN = "login_wk";

	// Login Table Columns names
	private static final String KEY_ID = "id";
	private static final String KEY_USERNAME = "username";
	private static final String KEY_NAME = "name";
	private static final String KEY_EMAIL = "email";
	private static final String KEY_PHONE = "phone";
	private static final String KEY_UID = "uid";
	private static final String KEY_CREATED_AT = "created_at";

	private static DatabaseHandler db = null;

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}


	public static DatabaseHandler getDatabaseHandler(Context context) {
		if(db == null) {
			db = new DatabaseHandler(context.getApplicationContext());
		}
		return db;
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_LOGIN + "("
				+ KEY_ID + " INTEGER PRIMARY KEY,"
				+ KEY_NAME + " TEXT,"
				+ KEY_USERNAME + " TEXT,"
				+ KEY_EMAIL + " TEXT,"
				+ KEY_PHONE + " TEXT,"
				+ KEY_UID + " TEXT,"
				+ KEY_CREATED_AT + " TEXT" + ")";
		db.execSQL(CREATE_LOGIN_TABLE);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);

		// Create tables again
		onCreate(db);
	}

	/**
	 * Storing user details in database
	 * */
	public void addUser(String name, String username, String email, String phone, String uid, String created_at) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, name); // name
		values.put(KEY_USERNAME, username); // username
		values.put(KEY_EMAIL, email); // email
		values.put(KEY_PHONE, phone); // phone
		values.put(KEY_UID, uid); // Uid
		values.put(KEY_CREATED_AT, created_at); // Created At

		// Inserting Row
		db.insert(TABLE_LOGIN, null, values);
		db.close(); // Closing database connection
	}

	/**
	 * Getting user data from database
	 * */
	public HashMap<String, String> getUserDetails(){
		HashMap<String,String> user = new HashMap<String,String>();
		String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		// Move to first row
		cursor.moveToFirst();
		if(cursor.getCount() > 0){
			user.put("username", cursor.getString(1));
			user.put("school", cursor.getString(2));
			user.put("year", cursor.getString(3));
			user.put("alliance", cursor.getString(4));
			user.put("uid", cursor.getString(5));
			user.put("created_at", cursor.getString(6));
		}
		cursor.close();
		db.close();
		// return user
		return user;
	}

	/**
	 * Getting user login status
	 * return true if rows are there in table
	 * */
	public int getRowCount() {
		String countQuery = "SELECT  * FROM " + TABLE_LOGIN;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		int rowCount = cursor.getCount();
		db.close();
		cursor.close();


		// return row count
		return rowCount;
	}


	public String getName() {
		String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;
		SQLiteDatabase db = this.getReadableDatabase();
		String name = null;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			if(cursor.getCount() == 0){
				name = "None";
			} else {
				// Move to first row
				cursor.moveToFirst();
				name = cursor.getString(1);
			}
		} catch (CursorIndexOutOfBoundsException e) {
			e.printStackTrace();
			Log.e("OutOfBounds", e.toString());
		}

		cursor.close();
		//db.close();
		Log.e("name", name);
		// return name
		return name;
	}


	public String getUsername() {
		String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;
		SQLiteDatabase db = this.getReadableDatabase();
		String username = null;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			if(cursor.getCount() == 0){
				username = "None";
			} else {
				// Move to first row
				cursor.moveToFirst();
				username = cursor.getString(2);
			}
		} catch (CursorIndexOutOfBoundsException e) {
			e.printStackTrace();
			Log.e("OutOfBounds", e.toString());
		}

		cursor.close();
		//db.close();
		Log.e("username", username);
		// return username
		return username;
	}

	public String getEmail() {
		String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;
		SQLiteDatabase db = this.getReadableDatabase();
		String email = null;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			if(cursor.getCount() == 0){
				email = "None";
			} else {
				// Move to first row
				cursor.moveToFirst();
				email = cursor.getString(3);
			}
		} catch (CursorIndexOutOfBoundsException e) {
			e.printStackTrace();
			Log.e("OutOfBounds", e.toString());
		}

		cursor.close();
		//db.close();
		Log.e("email", email);
		// return email
		return email;
	}

	public String getPhone() {
		String selectQuery = "SELECT  * FROM " + TABLE_LOGIN;
		SQLiteDatabase db = this.getReadableDatabase();
		String phone = null;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			if(cursor.getCount() == 0){
				phone = "None";
			} else {
				// Move to first row
				cursor.moveToFirst();
				phone = cursor.getString(4);
			}
		} catch (CursorIndexOutOfBoundsException e) {
			e.printStackTrace();
			Log.e("OutOfBounds", e.toString());
		}

		cursor.close();
		//db.close();
		Log.e("phone", phone);
		// return phone
		return phone;
	}





	/**
	 * Re crate database
	 * Delete all tables and create them again
	 * */
	public void resetTables(){
		SQLiteDatabase db = this.getWritableDatabase();
		// Delete All Rows
		db.delete(TABLE_LOGIN, null, null);
		db.close();
	}

}
