package wka.com.wardkonnect;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentNotifications extends Fragment {

    static String NOTIFICATIONS = "notifications";
    static String NOTIFICATION_ID = "id";
    static String NOTIFICATION_USERNAME = "username";
    static String NOTIFICATION_NAME = "name";
    static String NOTIFICATION_POST_ID = "post_id";
    static String NOTIFICATION_STATUS = "status";
    static String NOTIFICATION_VERIFIED = "verified";
    static String NOTIFICATION_FOLLOWERS_COUNT = "followers_count";
    static String NOTIFICATION_FOLLOWING_COUNT = "following_count";
    static String NOTIFICATION_POSTS_COUNT = "posts_count";
    static String NOTIFICATION_PROFILE_BANNER = "profile_banner";
    static String NOTIFICATION_MESSAGE = "message";
    static String NOTIFICATION_TAG = "tag";
    static String NOTIFICATION_ACTION = "action";
    static String NOTIFICATION_POST_TEXT = "post_text";
    static String NOTIFICATION_POST_IMAGE = "post_image";
    static String NOTIFICATION_PROFILE_PIC = "profile_pic";
    static String NOTIFICATION_TIME = "time";


    LinearLayout linlaHeaderProgress;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    ImageButton back;

    public static final String TAG = "MyTag";

    ListView lv;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    ListViewAdapter_Notifications adapter;
    ArrayList<HashMap<String, String>> arraylist;

    String username;

    SwipeRefreshLayout mSwipeRefreshLayout;


    public FragmentNotifications() {
        // Required empty public constructor
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_notifications, container, false);


        linlaHeaderProgress = (LinearLayout) rootView.findViewById(R.id.linlaHeaderProgress);
        lv = (ListView) rootView.findViewById(R.id.lvActNotify);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getContext());
        // user already logged in show dash board
        username = db.getUsername();

        ConnectionDetector cd = new ConnectionDetector(getContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getContext(), "Couldn't refresh", Toast.LENGTH_SHORT).show();


        }
        else
        {

            sendRequest();

        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (!cd.isConnectingToInternet()) {



                    ThreadPool.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();

                        }
                    });

                    mSwipeRefreshLayout.setRefreshing(false);

                } else {
                    sendRequestRefresh();
                }

            }

        });


        return rootView;

    }


    private void sendRequest(){


        String JSON_URL = "http://wardkonnect.xyz/app/notifications/get_all_notifications.php?username="+username+"&page=1";
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getContext(),"Internet connection is down",Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getContext());
        DiskBasedCache cache = new DiskBasedCache(getContext().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(NOTIFICATIONS);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("post_id", jsonobject.getString("post_id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("message", jsonobject.getString("message"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("tag", jsonobject.getString("tag"));
                map.put("action", jsonobject.getString("action"));
                map.put("post_image", jsonobject.getString("post_image"));
                map.put("post_text", jsonobject.getString("post_text"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_Notifications(getContext(), arraylist);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);
    }


    private void sendRequestRefresh(){


        String JSON_URL = "http://wardkonnect.xyz/app/notifications/get_all_notifications.php?username="+username+"&page=1";
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONRef(response);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getContext(),"Internet connection is down",Toast.LENGTH_LONG).show();
                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getContext());
        DiskBasedCache cache = new DiskBasedCache(getContext().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);


        mSwipeRefreshLayout.setRefreshing(true);

    }


    private void showJSONRef(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(NOTIFICATIONS);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("post_id", jsonobject.getString("post_id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("message", jsonobject.getString("message"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("tag", jsonobject.getString("tag"));
                map.put("action", jsonobject.getString("action"));
                map.put("post_image", jsonobject.getString("post_image"));
                map.put("post_text", jsonobject.getString("post_text"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_Notifications(getContext(), arraylist);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);
    }

}
