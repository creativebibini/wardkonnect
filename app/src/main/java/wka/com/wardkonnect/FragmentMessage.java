package wka.com.wardkonnect;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMessage extends Fragment {

    static String MESSAGES = "messages";
    static String ID = "id";
    static String MESSAGE_TO_USERNAME = "to_username";
    static String MESSAGE_FROM_USERNAME = "from_username";
    static String MESSAGE_MESSAGE = "message";
    static String MESSAGE_DATE = "date";
    static String MESSAGE_COUNT = "count";
    static String MESSAGE_STATUS = "status";
    static String MESSAGE_REG_ID = "reg_id";
    static String MESSAGE_VERIFIED = "verified";
    static String MESSAGE_POSTS_COUNT = "posts_count";
    static String MESSAGE_FOLLOWING_COUNT = "following_count";
    static String MESSAGE_FOLLOWERS_COUNT = "followers_count";
    static String MESSAGE_PROFILE_PIC = "profile_pic";
    static String MESSAGE_PROFILE_BANNER = "profile_banner";


    static String PROFILE = "profile";

    String str_id;
    String str_reg_id;
    String str_message;

    LinearLayout linlaHeaderProgress;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    ImageButton back;

    public static final String TAG = "MyTag";

    ListView lv;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    ListViewAdapter_messages adapter;
    ArrayList<HashMap<String, String>> arraylist;

 //   TextView messagegeneral;

    String username;

    SwipeRefreshLayout mSwipeRefreshLayout;


    public FragmentMessage() {
        // Required empty public constructor
    }

    View rootView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_message, container, false);

        linlaHeaderProgress = (LinearLayout) rootView.findViewById(R.id.linlaHeaderProgress);
        lv = (ListView) rootView.findViewById(R.id.lvActNotify);

     //   messagegeneral = (TextView) rootView.findViewById(R.id.messagegeneral);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getContext());
        // user already logged in show dash board
        username = db.getUsername();


        ConnectionDetector cd = new ConnectionDetector(getContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getContext(), "Couldn't refresh", Toast.LENGTH_SHORT).show();


        }
        else
        {

            sendRequest();
            //sendRequestMass();

        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (!cd.isConnectingToInternet()) {



                    ThreadPool.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();

                        }
                    });

                    mSwipeRefreshLayout.setRefreshing(false);

                } else {
                    sendRequestRefresh();
                    //sendRequestMass();
                }

            }

        });


        return rootView;

    }




    private void sendRequest(){


        String JSON_URL = "http://wardkonnect.xyz/app/messages/home_get_chats.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getContext(),"Internet connection is down",Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getContext());
        DiskBasedCache cache = new DiskBasedCache(getContext().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(MESSAGES);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("from_username", jsonobject.getString("from_username"));
                map.put("to_username", jsonobject.getString("to_username"));
                map.put("message", jsonobject.getString("message"));
                map.put("count", jsonobject.getString("count"));
                map.put("status", jsonobject.getString("status"));
                map.put("reg_id", jsonobject.getString("reg_id"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("date", jsonobject.getString("date"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_messages(getContext(), arraylist);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);
    }


    private void sendRequestRefresh(){


        String JSON_URL = "http://wardkonnect.xyz/app/messages/home_get_chats.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONRef(response);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getContext(),"Internet connection is down",Toast.LENGTH_LONG).show();

                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getContext());
        DiskBasedCache cache = new DiskBasedCache(getContext().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);


        mSwipeRefreshLayout.setRefreshing(true);

    }


    private void showJSONRef(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(MESSAGES);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("from_username", jsonobject.getString("from_username"));
                map.put("to_username", jsonobject.getString("to_username"));
                map.put("message", jsonobject.getString("message"));
                map.put("count", jsonobject.getString("count"));
                map.put("status", jsonobject.getString("status"));
                map.put("reg_id", jsonobject.getString("reg_id"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("date", jsonobject.getString("date"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_messages(getContext(), arraylist);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);
    }



//    private void sendRequestMass(){
//
//
//        String JSON_URL = "http://wardkonnect.xyz/app/messages/get_general.php";
//        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                showJSONJ(response);
//            }
//        },
//
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        requestQueue.cancelAll(TAG);
//                        Toast.makeText(getActivity(), "Internet connection is down", Toast.LENGTH_LONG).show();
//
//
//                    }
//                });
//
//        stringRequest.setTag(TAG);
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
//        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        requestQueue = Volley.newRequestQueue(getContext());
//        DiskBasedCache cache = new DiskBasedCache(getActivity().getCacheDir(), 100 * 1024 * 1024);
//        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
//        requestQueue.start();
//        requestQueue.add(stringRequest);
//
//
//
//    }
//
//
//    private void showJSONJ(String json){
//        jsonobject = null;
//        arraylist = new ArrayList<HashMap<String, String>>();
//        try {
//            jsonobject = new JSONObject(json);
//            jsonarray = jsonobject.getJSONArray(PROFILE);
//
//            JSONObject c = jsonarray.getJSONObject(0);
//            // Storing  JSON item in a Variable
//            str_id = c.getString("id");
//            str_message = c.getString("message");
//
//
//
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//     //   messagegeneral.setText(str_message);
//
//
//
//    }

}
