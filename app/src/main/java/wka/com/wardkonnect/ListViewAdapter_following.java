package wka.com.wardkonnect;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pdee on 07/10/2017.
 */

public class ListViewAdapter_following extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();

    // Declare Variables
    TextView usernameTV;
    ImageView profilePicImg;

    String username;

    public ListViewAdapter_following (Context context,
                                 ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.likes_list_item, parent, false);
        // Get the position
        resultp = data.get(position);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();

        // Locate the TextViews in listview_item.xml
        usernameTV = (TextView) itemView.findViewById(R.id.like_username);
        // Locate the ImageView in listview_item.xml
        profilePicImg = (ImageView) itemView.findViewById(R.id.like_dp);

        // Capture position and set results to the TextViews

        Picasso.with(context).load(resultp.get(Following.PROFILE_PIC)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                .into(profilePicImg);

        // Capture position and set results to the TextViews
        usernameTV.setText(resultp.get(Following.USERNAME));


        final Intent user_profile = new Intent(context, ProfileView.class);
        final Intent main_profile = new Intent(context, Profile.class);
        // Capture ListView item click
        itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);
                if(resultp.get(Following.USERNAME).toString().equalsIgnoreCase(username)){


                    main_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    main_profile.putExtra("id", resultp.get(Following.ID));
                    main_profile.putExtra("username", resultp.get(Following.USERNAME));
                    main_profile.putExtra("status", resultp.get(Following.STATUS));
                    main_profile.putExtra("verified", resultp.get(Following.VERIFIED));
                    main_profile.putExtra("followers_count", resultp.get(Following.FOLLOWERS_COUNT));
                    main_profile.putExtra("following_count", resultp.get(Following.FOLLOWING_COUNT));
                    main_profile.putExtra("posts_count", resultp.get(Following.POSTS_COUNT));
                    main_profile.putExtra("profile_pic", resultp.get(Following.PROFILE_PIC));
                    main_profile.putExtra("profile_banner", resultp.get(Following.PROFILE_BANNER));


                    context.startActivity(main_profile);
                }

                else{
                    user_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    user_profile.putExtra("id", resultp.get(Following.ID));
                    user_profile.putExtra("username", resultp.get(Following.USERNAME));
                    user_profile.putExtra("status", resultp.get(Following.STATUS));
                    user_profile.putExtra("verified", resultp.get(Following.VERIFIED));
                    user_profile.putExtra("followers_count", resultp.get(Following.FOLLOWERS_COUNT));
                    user_profile.putExtra("following_count", resultp.get(Following.FOLLOWING_COUNT));
                    user_profile.putExtra("posts_count", resultp.get(Following.POSTS_COUNT));
                    user_profile.putExtra("profile_pic", resultp.get(Following.PROFILE_PIC));
                    user_profile.putExtra("profile_banner", resultp.get(Following.PROFILE_BANNER));


                    context.startActivity(user_profile);
                }


            }
        });

        return itemView;
    }



}



