package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EditRegion extends AppCompatActivity {

    ImageButton back;

   // EditText regionET;
    ListView regionList;



    static String ID = "id";
    static String REGION_NAME = "region";


    LinearLayout linlaHeaderProgress;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListViewAdapter_region adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    RequestQueue requestQueue;
    StringRequest stringRequest;
    public static final String TAG = "CommPost";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_region);

        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);

        back = (ImageButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


       // regionET = (EditText) findViewById(R.id.professionET);
        regionList = (ListView) findViewById(R.id.regionList);



        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

            return;
        }


        else
        {
            sendRequest();
        }




    }


    private void sendRequest(){



        String JSON_URL = "http://wardkonnect.xyz/app/profile/get_region.php";
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EditRegion.this,"Internet connection is down",Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("region");

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("region", jsonobject.getString("region"));

                // Set the JSON Objects into the array



                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_region(EditRegion.this, arraylist);
        // Set the adapter to the ListView
        regionList.setAdapter(adapter);
    }


}
