package wka.com.wardkonnect;

/**
 * Created by itse-ugcs on 6/14/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewAdapter_profession extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();

    // Declare Variables

    TextView title;

    String username;

    public ListViewAdapter_profession(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.profession_list_item, parent, false);
        // Get the position
        resultp = data.get(position);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();

        // Locate the TextViews in listview_item.xml
        title = (TextView) itemView.findViewById(R.id.titleTV);



        // Capture position and set results to the TextViews
        title.setText(resultp.get(EditProfession.PROFESSION_NAME));


        final Intent main_profile = new Intent(context, SelectedProfession.class);
        // Capture ListView item click
        itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);

                main_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                main_profile.putExtra("name", resultp.get(EditProfession.PROFESSION_NAME));


                context.startActivity(main_profile);

            }
        });

        return itemView;
    }



}



