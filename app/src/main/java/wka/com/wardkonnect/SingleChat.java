package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SingleChat extends AppCompatActivity {


    static String MESSAGES = "messages";
    static String ID = "id";
    static String MESSAGE_TO_USERNAME = "to_username";
    static String MESSAGE_FROM_USERNAME = "from_username";
    static String MESSAGE_MESSAGE = "message";
    static String MESSAGE_DATE = "date";
    static String MESSAGE_COUNT = "count";
    static String MESSAGE_STATUS = "status";
    static String MESSAGE_VERIFIED = "verified";
    static String MESSAGE_POSTS_COUNT = "posts_count";
    static String MESSAGE_FOLLOWING_COUNT = "following_count";
    static String MESSAGE_FOLLOWERS_COUNT = "followers_count";
    static String MESSAGE_PROFILE_PIC = "profile_pic";
    static String MESSAGE_PROFILE_BANNER = "profile_banner";

    ImageView profilepic;
    TextView username_tv;
    ImageButton back;

    ImageButton btnSend;
    EditText comment_box;
    ListView lv;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListViewAdapter_single_message adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    String str_username;
    String str_profile_picture;
    String str_reg_id;

    String username;

    LinearLayout linlaHeaderProgress;

    RelativeLayout relativeLayout6;

    RequestQueue requestQueue;
    StringRequest stringRequest;
    public static final String TAG = "CommPost";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        setContentView(R.layout.activity_single_chat);




        username_tv = (TextView) findViewById(R.id.username);
        profilepic = (ImageView) findViewById(R.id.profilepic);
        back = (ImageButton) findViewById(R.id.back);

        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        lv = (ListView)findViewById(R.id.chatListview);
        btnSend = (ImageButton)findViewById(R.id.messageSend);
        btnSend.setClickable(true);
        comment_box = (EditText) findViewById(R.id.messageET);
        relativeLayout6 = (RelativeLayout) findViewById(R.id.relativeLayout6);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getApplicationContext());
        // user already logged in show dash board
        username = db.getUsername();

        Intent i = getIntent();
        str_username = i.getStringExtra("username");
        str_profile_picture = i.getStringExtra("profile_pic");
        str_reg_id = i.getStringExtra("reg_id");

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

            return;
        }


        else
        {
            sendRequest();
        }


        if(str_username.equalsIgnoreCase("wardkonnect")){

            relativeLayout6.setVisibility(View.GONE);
        }

        else {

            relativeLayout6.setVisibility(View.VISIBLE);

        }


        final Handler handler = new Handler();

        Runnable refresh = new Runnable() {
            @Override
            public void run() {
                sendRequestRef();
                handler.postDelayed(this, 5 * 1000);
            }
        };

        handler.postDelayed(refresh, 5 * 1000);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        Picasso.with(this).load(str_profile_picture).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.placeholder).transform(new RoundedCornersTransform())
                .into(profilepic);

        username_tv.setText(str_username);


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_SHORT).show();

                PostMessage();

            }
        });


        btnSend.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (comment_box.getText().toString().trim().length() > 0) {

                     ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    if (!cd.isConnectingToInternet()) {

                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

                        return;
                    }
                    else
                    {
                        PostMessage();



                    }


                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please add a message", Toast.LENGTH_SHORT)
                            .show();


                }



            }
        });

    }

    private void sendRequest(){


        String JSON_URL = "http://wardkonnect.xyz/app/messages/home_get_single_chats.php?to="+username+"&from="+str_username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
                btnSend.setVisibility(View.VISIBLE);

            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getApplicationContext(),"Internet connection is down",Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);
                        btnSend.setVisibility(View.VISIBLE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(this);
        DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        linlaHeaderProgress.setVisibility(View.VISIBLE);
        btnSend.setVisibility(View.GONE);
        //linlaHeaderProgress.setVisibility(View.GONE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(MESSAGES);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("from_username", jsonobject.getString("from_username"));
                map.put("to_username", jsonobject.getString("to_username"));
                map.put("message", jsonobject.getString("message"));
                map.put("count", jsonobject.getString("count"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("date", jsonobject.getString("date"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_single_message(SingleChat.this, arraylist);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);
    }



    private void sendRequestRef(){


        String JSON_URL = "http://wardkonnect.xyz/app/messages/home_get_single_chats.php?to="+username+"&from="+str_username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONT(response);
//                linlaHeaderProgress.setVisibility(View.GONE);
//                btnSend.setVisibility(View.VISIBLE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getApplicationContext(),"Internet connection is down",Toast.LENGTH_LONG).show();
                      //  linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(this);
        DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



      //  linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSONT(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(MESSAGES);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("from_username", jsonobject.getString("from_username"));
                map.put("to_username", jsonobject.getString("to_username"));
                map.put("message", jsonobject.getString("message"));
                map.put("count", jsonobject.getString("count"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("date", jsonobject.getString("date"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_single_message(SingleChat.this, arraylist);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);
    }


    public static String forJSON(String aText){
        final StringBuilder result = new StringBuilder();
        StringCharacterIterator iterator = new StringCharacterIterator(aText);
        char character = iterator.current();
        while (character != StringCharacterIterator.DONE){
            if( character == '\"' ){
                result.append("\\\"");
            }
            else if(character == '\\'){
                result.append("\\\\");
            }
            else if(character == '/'){
                result.append("\\/");
            }
            else if(character == '\''){
                result.append("\\\'");
            }
            else if(character == '\b'){
                result.append("\\b");
            }
            else if(character == '\f'){
                result.append("\\f");
            }
            else if(character == '\n'){
                result.append("\\n");
            }
            else if(character == '\r'){
                result.append("\\r");
            }
            else if(character == '\t'){
                result.append("\\t");
            }
            else {
                //the char is not a special one
                //add it to the result as is
                result.append(character);
            }
            character = iterator.next();
        }
        return result.toString();
    }


    private void PostMessage() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/messages/chat_post_message.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                       // Toast.makeText(getApplicationContext(),"Message Added",Toast.LENGTH_SHORT).show();

                        sendNotification();

                        comment_box.setText("");
                    //    btnComment.setEnabled(true);
                     //   mProgressDialog.dismiss();

                        btnSend.setVisibility(View.VISIBLE);
                        linlaHeaderProgress.setVisibility(View.GONE);

                            sendRequest();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String  comment = comment_box.getText().toString();
                String message_raw = StringEscapeUtils.escapeJava(comment);
                String new_comment= forJSON(message_raw);

                params.put("from_username", username);
                params.put("to_username", str_username);
                params.put("message", new_comment);

                return params;
            }
        };

        requestQueue.add(stringRequest);
        //Toast.makeText(getApplicationContext(),"Please wait...Adding Comment",Toast.LENGTH_SHORT).show();
//        mProgressDialog = new ProgressDialog(SingleChat.this);
//        // Set progressdialog message
//        mProgressDialog.setMessage("Posting message...Please wait");
//        mProgressDialog.setIndeterminate(false);
//        mProgressDialog.setCancelable(false);
//        // Show progressdialog
//        mProgressDialog.show();

        linlaHeaderProgress.setVisibility(View.VISIBLE);
        btnSend.setVisibility(View.GONE);
      //  btnComment.setEnabled(false);

    }

    private void sendNotification() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/notifications/send_ios_notification.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
                UserFunctions  userFunctions = new UserFunctions();
                userFunctions.isUserLoggedIn(getApplicationContext());
                // user already logged in show dash board
                String user = db.getUsername();

                String message = user+" sent you a message";

                params.put("message", message);
                params.put("regId", str_reg_id);

                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


}
