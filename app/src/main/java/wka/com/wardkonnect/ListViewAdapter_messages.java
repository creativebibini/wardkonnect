package wka.com.wardkonnect;

/**
 * Created by itse-ugcs on 6/14/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewAdapter_messages extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();

    // Declare Variables
    TextView usernameTV;
    TextView count;
    ImageView profilePicImg;
    TextView date;
    TextView message;

    String username;

    public ListViewAdapter_messages(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.messages_list_item, parent, false);
        // Get the position
        resultp = data.get(position);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();

        // Locate the TextViews in listview_item.xml
        usernameTV = (TextView) itemView.findViewById(R.id.username);
        message = (TextView) itemView.findViewById(R.id.message);
        date = (TextView) itemView.findViewById(R.id.date);
        // Locate the ImageView in listview_item.xml
        profilePicImg = (ImageView) itemView.findViewById(R.id.profilepic);
        count = (TextView) itemView.findViewById(R.id.count);


        message.setText(AndroidEmoji.ensure(resultp.get(FragmentMessage.MESSAGE_MESSAGE), context));


        Picasso.with(context).load(resultp.get(FragmentMessage.MESSAGE_PROFILE_PIC)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                .into(profilePicImg);

        usernameTV.setText(resultp.get(FragmentMessage.MESSAGE_FROM_USERNAME));
        date.setText(resultp.get(FragmentMessage.MESSAGE_DATE));

        count.setText(resultp.get(FragmentMessage.MESSAGE_COUNT));

        //final Intent user_profile = new Intent(context, ProfileView.class);
        final Intent main_profile = new Intent(context, SingleChat.class);
        // Capture ListView item click
        itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);

                    main_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    main_profile.putExtra("id", resultp.get(FragmentMessage.ID));
                    main_profile.putExtra("username", resultp.get(FragmentMessage.MESSAGE_FROM_USERNAME));
                    main_profile.putExtra("status", resultp.get(FragmentMessage.MESSAGE_STATUS));
                    main_profile.putExtra("reg_id", resultp.get(FragmentMessage.MESSAGE_REG_ID));
                    main_profile.putExtra("verified", resultp.get(FragmentMessage.MESSAGE_VERIFIED));
                    main_profile.putExtra("followers_count", resultp.get(FragmentMessage.MESSAGE_FOLLOWERS_COUNT));
                    main_profile.putExtra("following_count", resultp.get(FragmentMessage.MESSAGE_FOLLOWING_COUNT));
                    main_profile.putExtra("posts_count", resultp.get(FragmentMessage.MESSAGE_POSTS_COUNT));
                    main_profile.putExtra("profile_pic", resultp.get(FragmentMessage.MESSAGE_PROFILE_PIC));
                    main_profile.putExtra("profile_banner", resultp.get(FragmentMessage.MESSAGE_PROFILE_BANNER));

                    context.startActivity(main_profile);



            }
        });

        return itemView;
    }



}



