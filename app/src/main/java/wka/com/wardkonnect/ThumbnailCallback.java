package wka.com.wardkonnect;

import com.zomato.photofilters.imageprocessors.Filter;


public interface ThumbnailCallback {

    void onThumbnailClick(Filter filter);
}