package wka.com.wardkonnect.ui;

import wka.com.wardkonnect.models.Photo;

/**
 * Created by takusemba on 2017/09/11.
 */

public interface OnPhotoClickListener {

    void onPhotoClicked(Photo photo);
}
