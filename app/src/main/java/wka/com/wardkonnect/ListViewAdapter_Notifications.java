package wka.com.wardkonnect;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by pdee on 01/11/2016.
 */
public class ListViewAdapter_Notifications extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();




    static class ViewHolder {

        // Declare Variables
        TextView home_item_username;
        //ImageView post_image;
        ImageView home_item_profile_picture;
        TextView home_item_post_time;
        TextView post_text;
        TextView notification_comment;
        ImageButton imageButton;


        TextView tagtxt;

        RelativeLayout not_rel;
    }

    ViewHolder holder;

    String username;

    public ListViewAdapter_Notifications(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View itemView, ViewGroup parent) {

        itemView = null;

        if (itemView == null) {

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        itemView = inflater.inflate(R.layout.list_item_notifications, parent, false);
        // Get the position
        resultp = data.get(position);
        holder = new ViewHolder();

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();


        // Locate the TextViews in listview_item.xml
        holder.home_item_username = (TextView) itemView.findViewById(R.id.sch_item_username);
        holder.post_text = (TextView) itemView.findViewById(R.id.tagtxt);
        holder.notification_comment = (TextView) itemView.findViewById(R.id.sch_item_post_text);
        holder.home_item_post_time = (TextView) itemView.findViewById(R.id.sch_item_post_time);
        // Locate the ImageView in listview_item.xml
        holder.home_item_profile_picture = (ImageView) itemView.findViewById(R.id.sch_item_profile_picture);
       // holder.post_image = (ImageView) itemView.findViewById(R.id.post_image);





        holder.imageButton = (ImageButton) itemView.findViewById(R.id.sch_item_like_btn);

        holder.not_rel = (RelativeLayout) itemView.findViewById(R.id.not_rel);


    }

    else{

        holder = (ViewHolder) itemView.getTag();
    }
        // Capture position and set results to the TextViews

        // comment_display.setText(resultp.get(Comments.COMMENT_COMMENT));


        holder.home_item_username.setText(resultp.get(FragmentNotifications.NOTIFICATION_USERNAME));

        holder.notification_comment.setText(AndroidEmoji.ensure(resultp.get(FragmentNotifications.NOTIFICATION_MESSAGE), context));
        holder.home_item_post_time.setText(resultp.get(FragmentNotifications.NOTIFICATION_ACTION));


        String tag_new = resultp.get(FragmentNotifications.NOTIFICATION_TAG).toString();

        if(tag_new.equalsIgnoreCase("post_like")){

            holder.post_text.setText(AndroidEmoji.ensure(resultp.get(FragmentNotifications.NOTIFICATION_POST_TEXT), context));

        }



        else if(tag_new.equalsIgnoreCase("comment")){

            holder.post_text.setText(AndroidEmoji.ensure(resultp.get(FragmentNotifications.NOTIFICATION_MESSAGE), context));

        }


        final Intent user_pro = new Intent(context, ProfileView.class);
        holder.home_item_profile_picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultp = data.get(position);

                user_pro.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                user_pro.putExtra("id", resultp.get(FragmentNotifications.NOTIFICATION_ID));
                user_pro.putExtra("username", resultp.get(FragmentNotifications.NOTIFICATION_USERNAME));
                user_pro.putExtra("name", resultp.get(FragmentNotifications.NOTIFICATION_NAME));
                user_pro.putExtra("status", resultp.get(FragmentNotifications.NOTIFICATION_STATUS));
                user_pro.putExtra("verified", resultp.get(FragmentNotifications.NOTIFICATION_VERIFIED));
                user_pro.putExtra("followers_count", resultp.get(FragmentNotifications.NOTIFICATION_FOLLOWERS_COUNT));
                user_pro.putExtra("following_count", resultp.get(FragmentNotifications.NOTIFICATION_FOLLOWING_COUNT));
                user_pro.putExtra("posts_count", resultp.get(FragmentNotifications.NOTIFICATION_POSTS_COUNT));
                user_pro.putExtra("profile_pic", resultp.get(FragmentNotifications.NOTIFICATION_PROFILE_PIC));
                user_pro.putExtra("profile_banner", resultp.get(FragmentNotifications.NOTIFICATION_PROFILE_BANNER));



                context.startActivity(user_pro);

            }
        });

        Picasso.with(context).load(resultp.get(FragmentNotifications.NOTIFICATION_PROFILE_PIC)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                .transform(new RoundedCornersTransform()).into(holder.home_item_profile_picture);

//        Picasso.with(context).load(resultp.get(Notifications.NOTIFICATION_POST_IMAGE)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
//                .into(holder.post_image);


        final String tag = resultp.get(FragmentNotifications.NOTIFICATION_TAG).toString();

         String imagetag = "post_like";



        if(tag.equalsIgnoreCase("post_like")){

            holder.imageButton.setImageResource(R.drawable.ic_favorite_black_24dp1);
            holder.imageButton.setVisibility(View.VISIBLE);
            holder.notification_comment.setVisibility(View.GONE);

        }



        else if(tag.equalsIgnoreCase("comment")){

            holder.imageButton.setImageResource(R.drawable.ic_message_black_24dp);
            holder.imageButton.setVisibility(View.VISIBLE);

        }


        else if(tag.equalsIgnoreCase("follow")){

            holder.not_rel.setVisibility(View.GONE);

        }


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultp = data.get(position);
                if (tag.equalsIgnoreCase("post_like")) {

                    Intent hp = new Intent(context, View_post_notifications.class);
                    hp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    hp.putExtra("username", username);
                    hp.putExtra("post_id", resultp.get(FragmentNotifications.NOTIFICATION_POST_ID));
                    context.startActivity(hp);

                } else if (tag.equalsIgnoreCase("comment")) {


                    Intent hp = new Intent(context, View_post_notifications.class);
                    hp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    hp.putExtra("username", username);
                    hp.putExtra("post_id", resultp.get(FragmentNotifications.NOTIFICATION_POST_ID));
                    context.startActivity(hp);

                } else if (tag.equalsIgnoreCase("new_post")) {


                    Intent hp = new Intent(context, View_post_notifications.class);
                    hp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    hp.putExtra("username", username);
                    hp.putExtra("post_id", resultp.get(FragmentNotifications.NOTIFICATION_POST_ID));
                    context.startActivity(hp);

                } else if (tag.equalsIgnoreCase("follow")) {


                    Intent hp = new Intent(context, ProfileView.class);
                    hp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    hp.putExtra("username", resultp.get(FragmentNotifications.NOTIFICATION_USERNAME));
                    // hp.putExtra("post_id",resultp.get(Notifications.NOTIFICATION_POST_ID));
                    context.startActivity(hp);

                } else {


                }

            }


        });




        return itemView;
    }



}