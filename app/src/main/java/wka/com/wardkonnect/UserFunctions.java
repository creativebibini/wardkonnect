package wka.com.wardkonnect;

import android.content.Context;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Random;

public class UserFunctions {

	private JSONParser jsonParser;


	private static String loginURL =    "http://wardkonnect.xyz/app/index.php";
	private static String registerURL = "http://wardkonnect.xyz/app/index.php";

	private static String login_tag = "login";
	private static String register_tag = "register";


	// constructor
	public UserFunctions(){


		jsonParser = new JSONParser();
	}

	private static final int MAX_ATTEMPTS = 5;
	private static final int BACKOFF_MILLI_SECONDS = 2000;
	private static final Random random = new Random();

	/**
	 * function make Login Request
	 * @param username
	 * @param password
	 * */

	@SuppressWarnings("deprecation")

	public JSONObject loginUser(String username, String password){
		// Building Parameters
		HashMap<String, String> params = new HashMap<>();
		params.put("tag", login_tag);
		params.put("username", username);
		params.put("password", password);
		JSONObject json = null;
		try {
			json = jsonParser.makeHttpRequest(loginURL, "POST", params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return json
		// Log.e("JSON", json.toString());
		return json;
	}

	/**
	 * function make SignUp Request
	 * @param name
	 * @param username
	 * @param email
	 * @param phone
	 * @param password
	 * */
	@SuppressWarnings("deprecation")

	public JSONObject registerUser(String name, String username, String email, String phone, String password){
		// Building Parameters
		HashMap<String, String> params = new HashMap<>();
		params.put("tag", register_tag);
		params.put ("name", name);
		params.put("username", username);
		params.put("email", email);
		params.put("phone", phone);
		params.put ("password", password);

		// getting JSON Object
		JSONObject json = null;
		try {
			//json = jsonParser.getJSONFromUrl(registerURL, params);
			 json = jsonParser.makeHttpRequest(registerURL, "POST", params);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// return json
		return json;
	}


//	public JSONObject isUser(String username, String school){
//		// Building Parameters
//		HashMap<String, String> params = new HashMap<>();
//		params.put ("username", username);
//		params.put("school", school);
//
//		// getting JSON Object
//		JSONObject json = null;
//		try {
//			//json = jsonParser.getJSONFromUrl(registerURL, params);
//			json = jsonParser.makeHttpRequest("http://dentymz.com/appx/dtbcknd/follow/user_ff.php", "POST", params);
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		// return json
//		return json;
//	}





	/**
	 * Function get Login status
	 * */
	public boolean isUserLoggedIn(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		int count = db.getRowCount();
		if(count > 0){
			// user logged in

			return true;
		}
		return false;
	}

	/**
	 * Function to logout user
	 * Reset Database
	 * */
	public boolean logoutUser(Context context){
		DatabaseHandler db = new DatabaseHandler(context);
		db.resetTables();
		return true;
	}

}
