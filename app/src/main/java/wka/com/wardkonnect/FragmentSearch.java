package wka.com.wardkonnect;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentSearch extends Fragment {

    JSONObject jsonobject;
    JSONArray jsonarray;
    GridViewAdapter_search_box adap;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    GridView gridView;
    GridView listGrid;

    static String POSTS = "posts";
    static String POST_ID = "id";
    static String POST_USERNAME = "username";
    static String POST_NAME = "name";
    static String POST_PROFILE_PIC = "profile_pic";
    static String POST_PROFILE_BANNER = "profile_banner";
    static String POST_FOLLOWERS_CT = "followers_count";
    static String POST_FOLLOWING_CT = "following_count";
    static String POST_POSTS_CT = "posts_count";
    static String POST_PHOTO = "photo";
    static String POST_TEXT = "text";
    static String POST_TIME = "time";
    static String POST_STATUS = "status";
    static String POST_VERIFIED = "verified";
    static String POST_COMMENTS_COUNT = "comments_count";
    static String POST_LIKES_COUNT = "likes_count";
    static String POST_LIKED_BY = "liked_by";
    static String POST_REG_ID = "reg_id";

    LinearLayout linlaHeaderProgress;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    ImageView add;

    SwipeRefreshLayout mSwipeRefreshLayout;

    public static final String TAG = "MyTag";

    String username;

    // ImageButton search;
    EditText searchEt;

    String searchstr;


    public FragmentSearch() {
        // Required empty public constructor
    }

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_search, container, false);


        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getActivity());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getActivity());
        // user already logged in show dash board
        username = db.getUsername();

        searchEt = (EditText)rootView.findViewById(R.id.searchEt);


        searchstr = searchEt.getText().toString();

        searchEt.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {

                    Intent search = new Intent(getActivity(),SearchActivityResults.class);
                    search.putExtra("searchstr",searchEt.getText().toString());
                    startActivity(search);

                    return true;
                }
                return false;
            }
        });


        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (!cd.isConnectingToInternet()) {



                    ThreadPool.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();

                        }
                    });

                    mSwipeRefreshLayout.setRefreshing(false);

                } else {
                    sendRequestRefresh();
                }

            }

        });


        listGrid = (GridView) rootView.findViewById(R.id.listGrid);

        linlaHeaderProgress = (LinearLayout) rootView.findViewById(R.id.linlaHeaderProgress);

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();

        }
        else
        {
            sendRequestSq();
        }

        return rootView;
    }

    private void sendRequestSq(){


        String JSON_URL = "http://wardkonnect.xyz/app/search/searchresults_more.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONSq(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getActivity(), "Internet connection is down", Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getActivity());
        DiskBasedCache cache = new DiskBasedCache(getActivity().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSONSq(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("posts");

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("photo", jsonobject.getString("photo"));
                map.put("text", jsonobject.getString("text"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("comments_count", jsonobject.getString("comments_count"));
                map.put("likes_count", jsonobject.getString("likes_count"));
                map.put("liked_by", jsonobject.getString("liked_by"));
                map.put("reg_id", jsonobject.getString("reg_id"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adap = new GridViewAdapter_search_box(getActivity(), arraylist);
        // Set the adapter to the ListView
        listGrid.setAdapter(adap);
    }


    private void sendRequestRefresh(){


        String JSON_URL = "http://wardkonnect.xyz/app/search/searchresults_more.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONRef(response);
             //   linlaHeaderProgress.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getActivity(), "Internet connection is down", Toast.LENGTH_LONG).show();
                 //       linlaHeaderProgress.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setRefreshing(false);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getActivity());
        DiskBasedCache cache = new DiskBasedCache(getActivity().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        mSwipeRefreshLayout.setRefreshing(true);
    }


    private void showJSONRef(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("posts");

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("photo", jsonobject.getString("photo"));
                map.put("text", jsonobject.getString("text"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("comments_count", jsonobject.getString("comments_count"));
                map.put("likes_count", jsonobject.getString("likes_count"));
                map.put("liked_by", jsonobject.getString("liked_by"));
                map.put("reg_id", jsonobject.getString("reg_id"));


                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adap = new GridViewAdapter_search_box(getActivity(), arraylist);
        // Set the adapter to the ListView
        listGrid.setAdapter(adap);
    }



}
