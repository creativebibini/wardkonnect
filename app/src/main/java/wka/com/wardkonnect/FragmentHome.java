package wka.com.wardkonnect;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment {

    FloatingActionButton post;

    String str_id;
    String str_username;
    String str_name;
    String str_location;
    String str_url;
     String str_suspended;
    String str_followersct;
    String str_followingct;
    String str_postsct;
    String str_status;
    String str_profile_picture;
    String str_profile_banner;
    String str_profile_verified;

    static String PROFILE = "profile";
    static String PROFILE_ID = "id";
    static String PROFILE_STATUS = "status";
    static String PROFILE_FOLLOWERS = "followers_count";
    static String PROFILE_FOLLOWING = "following_count";
    static String PROFILE_POSTS = "posts_count";
    static String PROFILE_PICTURE = "profile_picture";
    static String PROFILE_BANNER = "profile_banner";

    public static final String TAG = "MyTag";

    static String POSTS = "posts";
    static String POST_ID = "id";
    static String POST_USERNAME = "username";
    static String POST_NAME = "name";
    static String POST_PROFILE_PIC = "profile_pic";
    static String POST_PROFILE_BANNER = "profile_banner";
    static String POST_FOLLOWERS_CT = "followers_count";
    static String POST_FOLLOWING_CT = "following_count";
    static String POST_POSTS_CT = "posts_count";
    static String POST_PHOTO = "photo";
    static String POST_TEXT = "text";
    static String POST_TIME = "time";
    static String POST_STATUS = "status";
    static String POST_VERIFIED = "verified";
    static String POST_SUSPENDED = "suspended";
    static String POST_COMMENTS_COUNT = "comments_count";
    static String POST_LIKES_COUNT = "likes_count";
    static String POST_LIKED_BY = "liked_by";
    static String POST_REG_ID = "reg_id";
    static String POST_TAG = "tag";

    SwipeRefreshLayout mSwipeRefreshLayout;


    String str_text;
    String str_image;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    ListViewAdapter_Home adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    LinearLayout linlaHeaderProgress;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    private static final int REQUEST_CODE = 100;

    public FragmentHome() {
        // Required empty public constructor
    }


    View rootView;

    ListView lv;
    RelativeLayout suspended;

    String username;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);


        post = (FloatingActionButton) rootView.findViewById(R.id.fab);

        linlaHeaderProgress = (LinearLayout) rootView.findViewById(R.id.linlaHeaderProgress);
        lv = (ListView) rootView.findViewById(R.id.listview_home);
        suspended = (RelativeLayout) rootView.findViewById(R.id.suspended);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getContext());
        // user already logged in show dash board
        username = db.getUsername();


        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                startActivityForResult(new Intent(getActivity(), Post.class), REQUEST_CODE);

            }
        });

        ConnectionDetector cd = new ConnectionDetector(getActivity());
        if (!cd.isConnectingToInternet()) {



            ThreadPool.runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Toast.makeText(getActivity(), "Couldn't refresh", Toast.LENGTH_LONG).show();

                }
            });


        }
        else
        {

            sendRequest();
            sendRequesprofile();

        }


        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.activity_main_swipe_refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ConnectionDetector cd = new ConnectionDetector(getActivity());
                if (!cd.isConnectingToInternet()) {



                    ThreadPool.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_LONG).show();

                        }
                    });

                    mSwipeRefreshLayout.setRefreshing(false);

                } else {
                    sendRequestRefresh();
                    sendRequesprofile();
                }

            }

        });


        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQUEST_CODE) {

            ConnectionDetector cd = new ConnectionDetector(getActivity());
            if (!cd.isConnectingToInternet()) {


                ThreadPool.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Toast.makeText(getActivity(), "Failed to Update!", Toast.LENGTH_LONG).show();

                    }
                });

            }
            else
            {

                //    sendRequestRefresh();

                sendFeedREfresh();


            }

        }


    }


    private void sendRequest(){

        String JSON_URL = "http://wardkonnect.xyz/app/posts/get_home_posts.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        ThreadPool.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(getActivity(), "Internet connection is down", Toast.LENGTH_LONG).show();

                            }
                        });
                        linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getActivity());
        DiskBasedCache cache = new DiskBasedCache(getActivity().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(POSTS);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("photo", jsonobject.getString("photo"));
                map.put("text", jsonobject.getString("text"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("suspended", jsonobject.getString("suspended"));
                map.put("comments_count", jsonobject.getString("comments_count"));
                map.put("likes_count", jsonobject.getString("likes_count"));
                map.put("liked_by", jsonobject.getString("liked_by"));
                map.put("tag", jsonobject.getString("tag"));
                map.put("reg_id", jsonobject.getString("reg_id"));

                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }




        adapter = new ListViewAdapter_Home(getActivity(), arraylist, FragmentHome.this);
        // Set the adapter to the ListView
        lv.setAdapter(adapter);




    }


    public void sendRequestRefresh(){

        String JSON_URL = "http://wardkonnect.xyz/app/posts/get_home_posts.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONref(response);
               mSwipeRefreshLayout.setRefreshing(false);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        ThreadPool.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(getActivity(), "Internet connection is down", Toast.LENGTH_LONG).show();

                            }
                        });
                        linlaHeaderProgress.setVisibility(View.GONE);

                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getActivity());
        DiskBasedCache cache = new DiskBasedCache(getActivity().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        mSwipeRefreshLayout.setRefreshing(true);
    }


    private void showJSONref(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(POSTS);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("photo", jsonobject.getString("photo"));
                map.put("text", jsonobject.getString("text"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("suspended", jsonobject.getString("suspended"));
                map.put("comments_count", jsonobject.getString("comments_count"));
                map.put("likes_count", jsonobject.getString("likes_count"));
                map.put("liked_by", jsonobject.getString("liked_by"));
                map.put("tag", jsonobject.getString("tag"));
                map.put("reg_id", jsonobject.getString("reg_id"));

                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }






        adapter = new ListViewAdapter_Home(getActivity(), arraylist, FragmentHome.this);
        lv.setAdapter(adapter);


    }



    public void sendFeedREfresh(){

        String JSON_URL = "http://wardkonnect.xyz/app/posts/get_home_posts.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONRefFeed(response);
                //        mSwipeRefreshLayout.setRefreshing(false);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        ThreadPool.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(getActivity(), "Internet connection is down",
                                        Toast.LENGTH_LONG).show();

                            }
                        });
                        //  linlaHeaderProgress.setVisibility(View.GONE);



                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getActivity());
        DiskBasedCache cache = new DiskBasedCache(getActivity().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



        //   mSwipeRefreshLayout.setRefreshing(true);
    }


    private void showJSONRefFeed(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(POSTS);

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("photo", jsonobject.getString("photo"));
                map.put("text", jsonobject.getString("text"));
                map.put("time", jsonobject.getString("time"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("suspended", jsonobject.getString("suspended"));
                map.put("comments_count", jsonobject.getString("comments_count"));
                map.put("likes_count", jsonobject.getString("likes_count"));
                map.put("liked_by", jsonobject.getString("liked_by"));
                map.put("tag", jsonobject.getString("tag"));
                map.put("reg_id", jsonobject.getString("reg_id"));

                // Set the JSON Objects into the array
                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }








        // save index and top position
        int index = lv.getFirstVisiblePosition();
        View v = lv.getChildAt(0);
        int top = (v == null) ? 0 : v.getTop();

// notify dataset changed or re-assign adapter here
        adapter = new ListViewAdapter_Home(getActivity(), arraylist,FragmentHome.this);
        lv.setAdapter(adapter);
// restore the position of listview
        lv.setSelectionFromTop(index, top);

    }


    private void sendRequesprofile(){


        String JSON_URL = "http://wardkonnect.xyz/app/profile/get_prof_details.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONPro(response);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getActivity(), "Internet connection is down", Toast.LENGTH_LONG).show();


                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(getActivity());
        DiskBasedCache cache = new DiskBasedCache(getActivity().getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



    }


    private void showJSONPro(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(PROFILE);

            JSONObject c = jsonarray.getJSONObject(0);
            // Storing  JSON item in a Variable
            str_id = c.getString("id");
            str_username = c.getString("username");
            str_name = c.getString("name");
            str_status = c.getString("status");
            str_location = c.getString("location");
            str_url = c.getString("url");
            str_suspended = c.getString("suspended");
            str_followersct = c.getString("followers_count");
            str_followingct = c.getString("following_count");
            str_postsct = c.getString("posts_count");
            str_profile_picture = c.getString("profile_picture");
            str_profile_banner = c.getString("profile_banner");
            str_profile_verified = c.getString("verified");





        } catch (JSONException e) {
            e.printStackTrace();
        }



        if(str_suspended.equalsIgnoreCase("yes")){

            suspended.setVisibility(View.VISIBLE);
            post.setVisibility(View.GONE);
        }

        else {

            suspended.setVisibility(View.GONE);
            post.setVisibility(View.VISIBLE);

        }


    }




}
