package wka.com.wardkonnect;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Settings extends AppCompatActivity {

    ImageButton back;



    RelativeLayout rel_logout;
    RelativeLayout rel_change_pwd;
    RelativeLayout rel_tips;
    RelativeLayout rel_contact;
    RelativeLayout rel_terms;
    RelativeLayout rel_privacy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        back = (ImageButton) findViewById(R.id.back);

        rel_logout = (RelativeLayout) findViewById(R.id.rel_logout);
        rel_change_pwd = (RelativeLayout) findViewById(R.id.rel_change_pwd);
        rel_tips = (RelativeLayout) findViewById(R.id.rel_tips);
        rel_contact = (RelativeLayout) findViewById(R.id.rel_contact);
        rel_terms = (RelativeLayout) findViewById(R.id.rel_terms);
        rel_privacy = (RelativeLayout) findViewById(R.id.rel_privacy);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        rel_change_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Settings.this, ChangePassword.class);
                startActivity(in);
            }
        });


        rel_tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Settings.this, Tips.class);
                startActivity(in);
            }
        });


        rel_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Settings.this, Contact.class);
                startActivity(in);
            }
        });


        rel_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Settings.this, Terms.class);
                startActivity(in);
            }
        });

        rel_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Settings.this, Privacy.class);
                startActivity(in);
            }
        });


        rel_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Settings.this);
                builder.setTitle("Log Out");
                builder.setMessage("Do you really want to LogOut");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                if (!cd.isConnectingToInternet()) {

                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

                    return;
                }


                else
                {
                  //  UpdateRegId();

                    DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                    db.resetTables();

                    Intent login = new Intent(getApplicationContext(), Login.class);
                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    getApplicationContext().startActivity(login);
                    finish();
                }


                    }
                });

                builder.setNegativeButton("Cancel", null);
                builder.show();
            }
        });



    }
}
