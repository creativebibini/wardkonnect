package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Comments extends AppCompatActivity {

    static String ID = "id";
    static String COMMENT_USERNAME = "username";
    static String COMMENT_NAME = "name";
    static String COMMENT_COMMENT = "comment";
    static String COMMENT_DATE = "date";
    static String COMMENT_STATUS = "status";
    static String COMMENT_VERIFIED = "verified";
    static String COMMENT_POSTS_COUNT = "posts_count";
    static String COMMENT_FOLLOWING_COUNT = "following_count";
    static String COMMENT_FOLLOWERS_COUNT = "followers_count";
    static String COMMENT_PROFILE_PIC = "profile_pic";
    static String COMMENT_PROFILE_BANNER = "profile_banner";


    String str_id;
    String str_username;
    String str_name;
    String str_location;
    String str_url;
    String str_suspended;
    String str_followersct;
    String str_followingct;
    String str_postsct;
    String str_status;
    String str_profile_picture;
    String str_profile_banner;
    String str_profile_verified;

    static String PROFILE = "profile";

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListViewAdapter_comments adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    String post_id;
    String username;
    String user_involved;
   // String str_suspended;

    int current_page = 1;
    Button btnLoadMore;

    ImageButton btnComment;
    EditText comment_box;
    ListView listview;

    LinearLayout linlaHeaderProgress;

    RequestQueue requestQueue;
    StringRequest stringRequest;
    public static final String TAG = "CommPost";

    String reg_id;

    ImageButton back;

    RelativeLayout relativeLayout6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        setContentView(R.layout.activity_comments);

        back = (ImageButton) findViewById(R.id.back);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);

        relativeLayout6 = (RelativeLayout) findViewById(R.id.relativeLayout6);
        relativeLayout6.setVisibility(View.GONE);

        listview = (ListView)findViewById(R.id.commentsListview);
//        listview.setEmptyView(findViewById(android.R.id.empty));

        btnComment = (ImageButton)findViewById(R.id.commentSend);
        btnComment.setClickable(true);
        comment_box = (EditText) findViewById(R.id.commentET);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getApplicationContext());
        // user already logged in show dash board
        username = db.getUsername();


        Intent i = getIntent();
        // Get the result of rank
        post_id = i.getStringExtra("id");
        user_involved = i.getStringExtra("username");
        //str_suspended = i.getStringExtra("suspended");
        reg_id = i.getStringExtra("reg_id");


//        Toast.makeText(getApplicationContext(),
//                str_suspended, Toast.LENGTH_SHORT)
//                .show();




//        // Creating a button - Load More
//        btnLoadMore = new Button(HomePostsComments.this);
//        btnLoadMore.setText("Load More");
//        btnLoadMore.setBackgroundDrawable(null);
//
//        // Adding button to listview at footer
//        listview.addFooterView(btnLoadMore);
//
//        btnLoadMore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//
//

//
//
//            }
//        });

        btnComment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (comment_box.getText().toString().trim().length() > 0) {

                    // String comment = sup_comment_box.getText().toString();


                    ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                    if (!cd.isConnectingToInternet()) {

                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

                        return;
                    }
                    else
                    {
                        PostComment();
                    }


                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please comment", Toast.LENGTH_SHORT)
                            .show();


                }



            }
        });


        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

            return;
        }


        else
        {
            sendRequesprofile();
            sendRequest();

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }

        return false;
    }

    private void sendRequest(){



        String JSON_URL = "http://wardkonnect.xyz/app/comments/home_get_comments.php?id="+post_id+"&"+"page=1";
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Comments.this,"Internet connection is down",Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("comments");

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("comment", jsonobject.getString("comment"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("date", jsonobject.getString("date"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                // Set the JSON Objects into the array



                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_comments(Comments.this, arraylist);
        // Set the adapter to the ListView
        listview.setAdapter(adapter);
    }




    private void sendRequestMore(){

        current_page++;

        String JSON_URL = "http://wardkonnect.xyz/app/comments/home_get_comments.php?id="+post_id+"&"+"page="+current_page++;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONMore(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Comments.this,"Internet connection is down",Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        btnLoadMore.setText("Loading ...");
    }


    private void showJSONMore(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("comments");

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("name", jsonobject.getString("name"));
                map.put("school", jsonobject.getString("school"));
                map.put("comment", jsonobject.getString("comment"));
                map.put("date", jsonobject.getString("date"));
                map.put("year", jsonobject.getString("year"));
                map.put("bio", jsonobject.getString("bio"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                // Set the JSON Objects into the array



                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_comments(Comments.this, arraylist);
        int currentPosition = listview.getFirstVisiblePosition();
        listview.setAdapter(adapter);
        // Setting new scroll position
        listview.setSelectionFromTop(currentPosition + 1, 0);
        //linlaHeaderProgress.setVisibility(View.GONE);
        btnLoadMore.setText("Load More");
    }





    public static String forJSON(String aText){
        final StringBuilder result = new StringBuilder();
        StringCharacterIterator iterator = new StringCharacterIterator(aText);
        char character = iterator.current();
        while (character != StringCharacterIterator.DONE){
            if( character == '\"' ){
                result.append("\\\"");
            }
            else if(character == '\\'){
                result.append("\\\\");
            }
            else if(character == '/'){
                result.append("\\/");
            }
            else if(character == '\''){
                result.append("\\\'");
            }
            else if(character == '\b'){
                result.append("\\b");
            }
            else if(character == '\f'){
                result.append("\\f");
            }
            else if(character == '\n'){
                result.append("\\n");
            }
            else if(character == '\r'){
                result.append("\\r");
            }
            else if(character == '\t'){
                result.append("\\t");
            }
            else {
                //the char is not a special one
                //add it to the result as is
                result.append(character);
            }
            character = iterator.next();
        }
        return result.toString();
    }


    private void PostComment() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/comments/home_post_comment.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(),"Comment Added",Toast.LENGTH_SHORT).show();

                        sendNotification();

                        comment_box.setText("");
                        btnComment.setEnabled(true);
                        mProgressDialog.dismiss();


                        if(!username.toString().equalsIgnoreCase(user_involved)){

                            sendRequest();
                        //    new SendCommentNotification().execute();


                        }

                        else{

                            sendRequest();

                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String  comment = comment_box.getText().toString();
                String message_raw = StringEscapeUtils.escapeJava(comment);
                String new_comment= forJSON(message_raw);

                params.put("username", username);
                params.put("post_id", post_id);
                params.put("comment", new_comment);
                params.put("user_involved",user_involved);

                return params;
            }
        };
        requestQueue.add(stringRequest);
        //Toast.makeText(getApplicationContext(),"Please wait...Adding Comment",Toast.LENGTH_SHORT).show();
        mProgressDialog = new ProgressDialog(Comments.this);
        // Set progressdialog message
        mProgressDialog.setMessage("Posting comment...Please wait");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        // Show progressdialog
        mProgressDialog.show();

        btnComment.setEnabled(false);

    }

//    public class SendCommentNotification extends AsyncTask<String, String, String> {
//
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
//            UserFunctions  userFunctions = new UserFunctions();
//            userFunctions.isUserLoggedIn(getApplicationContext());
//            // user already logged in show dash board
//            String user = db.getUsername();
//
//            String message = user+"%20commented%20on%20your%20post";
//
//
//
//            HttpClient httpclient = new DefaultHttpClient();
//            // HttpPost httppost = new HttpPost("http://truebelieversworld.com/tb/notifications/send_message.php?regId="+reg_id+"&message="+message);
//            HttpPost httppost = new HttpPost("http://findclient.me/tb/firebase/index.php?title=TrueBelievers&message="+message+"&push_type=individual&regId="+reg_id);
//
//
//
//            try {
//
//                httpclient.execute(httppost);
//
//
//            } catch (NullPointerException e) {
//                e.printStackTrace();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(String lenghtOfFile) {
//
//        }
//
//    }


    private void sendRequesprofile(){


        String JSON_URL = "http://wardkonnect.xyz/app/profile/get_prof_details.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSONPro(response);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getApplicationContext(), "Internet connection is down", Toast.LENGTH_LONG).show();


                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(this);
        DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



    }


    private void showJSONPro(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(PROFILE);

            JSONObject c = jsonarray.getJSONObject(0);
            // Storing  JSON item in a Variable
            str_id = c.getString("id");
            str_username = c.getString("username");
            str_name = c.getString("name");
            str_status = c.getString("status");
            str_location = c.getString("location");
            str_url = c.getString("url");
            str_suspended = c.getString("suspended");
            str_followersct = c.getString("followers_count");
            str_followingct = c.getString("following_count");
            str_postsct = c.getString("posts_count");
            str_profile_picture = c.getString("profile_picture");
            str_profile_banner = c.getString("profile_banner");
            str_profile_verified = c.getString("verified");





        } catch (JSONException e) {
            e.printStackTrace();
        }



        if(str_suspended.equalsIgnoreCase("yes")){

            relativeLayout6.setVisibility(View.GONE);

        }

        else{

            relativeLayout6.setVisibility(View.VISIBLE);

        }


    }


    private void sendNotification() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/notifications/send_ios_notification.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
                UserFunctions  userFunctions = new UserFunctions();
                userFunctions.isUserLoggedIn(getApplicationContext());
                // user already logged in show dash board
                String user = db.getUsername();

                String message = user+" commented on your post";

                params.put("message", message);
                params.put("regId", reg_id);

                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

}
