package wka.com.wardkonnect;

public final class Config {

    private Config() {
    }

    public static final String DEVELOPER_KEY = "AIzaSyDf7ldRHyINpaKOdAKCa1QYjbNOS1Hpzjw";

    //school post with pic
    public static final String HOME_PIC_UPLOAD_URL = "http://wardkonnect.xyz/app/posts/home_post_pic.php";

    //school post no pic
    public static final String HOME_NOPIC_UPLOAD_URL = "http://wardkonnect.xyz/app/posts/home_post_no_pic.php";

    public static final String TESTIMONY_POST_URL = "http://wardkonnect.xyz/app/posts/testimony_post.php";

    public static final String PROFILE_PIC_UPLOAD_URL = "http://wardkonnect.xyz/app/profile/update_profile_pic.php";

    public static final String BANNER_PIC_UPLOAD_URL = "http://wardkonnect.xyz/app/profile/update_banner_pic.php";


    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "tb_firebase";

}


