package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Report extends AppCompatActivity {

    ImageButton back;

    Button btnSubmitReport;
    Spinner reportReason;
    Spinner reportPriority;

    String reason;
    String priority;
    String username;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    String post_id;

    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);


        back = (ImageButton)findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });



        prioritySpin();
        reasonSpin();

        btnSubmitReport = (Button)findViewById(R.id.reportBtn);
        btnSubmitReport.setClickable(true);
        reportReason = (Spinner)findViewById(R.id.repSpinReason);
        reportPriority = (Spinner)findViewById(R.id.repSpinPrior);

        Intent i = getIntent();
        // Get the result of rank
        post_id = i.getStringExtra("id");

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(getApplicationContext());
        // user already logged in show dash board
        username = db.getUsername();


        btnSubmitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(reason.equals("Select your Reason")){
                    Toast.makeText(getApplicationContext(), "Please Select your Reason", Toast.LENGTH_LONG).show();
                }

//                else if(priority.equals("Set your Priority")){
//                    Toast.makeText(getApplicationContext(), "Please Set your Priority", Toast.LENGTH_LONG).show();
//                }

                else {
                    PostReport();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                break;
        }

        return false;
    }


    public void reasonSpin(){

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Select your Reason");
        spinnerArray.add("This Photo poses threat");
        spinnerArray.add("This Photo contains nudity");;
        spinnerArray.add("This Photo is not suitable");





        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner yrspin = (Spinner) findViewById(R.id.repSpinReason);
        yrspin.setAdapter(adapter);

        yrspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {

                reason = parentView.getItemAtPosition(myPosition).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


    }


    public void prioritySpin(){

        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("Set your Priority");
        spinnerArray.add("5");
        spinnerArray.add("4");
        spinnerArray.add("3");
        spinnerArray.add("2");
        spinnerArray.add("1");





        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner yrspin = (Spinner) findViewById(R.id.repSpinPrior);
        yrspin.setAdapter(adapter);

        yrspin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int myPosition, long myID) {


                priority = parentView.getItemAtPosition(myPosition).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

    }

    private void PostReport() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/report/home_report.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        Toast.makeText(getApplicationContext(),response,Toast.LENGTH_SHORT).show();
                        finish();
                        mProgressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("username", username);
                params.put("post_id", post_id);
                params.put("reason", reason);
                params.put("priority","5" );

                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        //Toast.makeText(getApplicationContext(),"Please wait...Submitting Report",Toast.LENGTH_SHORT).show();
        mProgressDialog = new ProgressDialog(Report.this);
        // Set progressdialog message
        mProgressDialog.setMessage("Please wait...Submitting Report");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show();

    }
}
