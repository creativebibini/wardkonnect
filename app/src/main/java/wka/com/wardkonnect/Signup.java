package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Signup extends AppCompatActivity {




    // JSON Response node names
    private static String KEY_SUCCESS = "success";
    private static String KEY_ERROR = "error";
    private static String KEY_ERROR_MSG = "error_msg";
    private static String KEY_UID = "uid";
    private static String KEY_NAME = "name";
    private static String KEY_USERNAME = "username";
    private static String KEY_EMAIL = "email";
    private static String KEY_PHONE = "phone";
    private static String KEY_CREATED_AT = "created_at";

    EditText inputUsername;
    EditText inputName;
    EditText inputEmail;
    EditText inputPhone;
    EditText inputPassword;
    Button btnRegister;

    CheckBox terms;

    ImageButton back;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    ProgressDialog mProgressDialog;

    TextView termstv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        inputEmail = (EditText) findViewById(R.id.email_et);
        inputName = (EditText) findViewById(R.id.name_et);
        inputUsername = (EditText) findViewById(R.id.username_et);
        inputPassword = (EditText) findViewById(R.id.password_et);

        back = (ImageButton) findViewById(R.id.back);

        termstv = (TextView) findViewById(R.id.termstv);


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        terms = (CheckBox) findViewById(R.id.terms);

        btnRegister = (Button) findViewById(R.id.signup_btn);


        termstv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ter = new Intent(Signup.this, Terms.class);
                startActivity(ter);
            }
        });

        // Register Button Click event
        btnRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
                if (!cd.isConnectingToInternet()) {

                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();

                    return;
                }
                else
                {




                    String username = inputUsername.getText().toString();
                    String password = inputPassword.getText().toString();
                    String email = inputEmail.getText().toString();
                    String name = inputName.getText().toString();
                    String phone = "0";




                    if( ( inputUsername.getText().toString().equals("")) && ( inputPassword.getText().toString().equals(""))){

                        {
                            Toast.makeText(getApplicationContext(), "Fields can't be blank ", Toast.LENGTH_LONG).show();
                        }


                    }

                    else if(( inputUsername.getText().toString().equals("")) || ( inputPassword.getText().toString().equals(""))){

                        {
                            Toast.makeText(getApplicationContext(), "Please fill all fields", Toast.LENGTH_LONG).show();
                        }

                    }


                    else if(( !(inputUsername.length() > 2)) ){

                        {
                            // Toast.makeText(getApplicationContext(), "Please Enter suptag with * ", Toast.LENGTH_LONG).show();
                            inputUsername.setError("Username is too short");
                        }

                    }

                    else if(((inputUsername.length() > 15)) ){

                        {
                            // Toast.makeText(getApplicationContext(), "Please Enter suptag with * ", Toast.LENGTH_LONG).show();
                            inputUsername.setError("Username is too long");
                        }

                    }


                    else if(( !(inputPassword.length() > 6))){

                        {
                            // Toast.makeText(getApplicationContext(), "Please Enter suptag with * ", Toast.LENGTH_LONG).show();
                            inputPassword.setError("Password not strong");
                        }




                    }

                    else if((!terms.isChecked())){

                        {
                            // Toast.makeText(getApplicationContext(), "Please Enter suptag with * ", Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(),"Please Agree to our Terms and Conditions",Toast.LENGTH_LONG).show();
                        }




                    }


                    else {


                        Toast.makeText(getApplicationContext(),"Please wait",Toast.LENGTH_SHORT).show();
                       // linlaHeaderProgress.setVisibility(View.VISIBLE);
                        btnRegister.setText("Signing Up");


                        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/index.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        // response
                                        Log.d("Response", response);
                                        //   Toast.makeText(getApplicationContext(),response,Toast.LENGTH_SHORT).show();


                                        try {
                                            JSONObject jObj = new JSONObject(response);
                                            // boolean error = jObj.getBoolean("error");

                                            String res = jObj.getString(KEY_SUCCESS);

                                            // Check for error node in json
                                            if (Integer.parseInt(res) == 1) {
                                                // user successfully logged in

                                                // Now store the user in SQLite
                                                String uid = jObj.getString("uid");

                                                JSONObject user = jObj.getJSONObject("user");
                                                String name = user.getString("name");
                                                String username = user.getString("username");
                                                String email = user.getString("email");
                                                String phone = user.getString("phone");
                                                String created_at = user.getString("created_at");



                                                DatabaseHandler db = new DatabaseHandler(getApplicationContext());
                                                // Inserting row in users table
                                                db.addUser(name, username, email, phone, uid, created_at);

                                                // Launch main activity
                                                Intent intent = new Intent(Signup.this, MainActivity.class);
                                                startActivity(intent);
                                                finish();
                                            } else {
                                                // Error in login. Get the error message
                                                String errorMsg = jObj.getString("error_msg");
                                                Toast.makeText(getApplicationContext(),
                                                        errorMsg, Toast.LENGTH_LONG).show();
                                             //   linlaHeaderProgress.setVisibility(View.GONE);
                                            }
                                        } catch (JSONException e) {
                                            // JSON error
                                            e.printStackTrace();
                                            Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                            //linlaHeaderProgress.setVisibility(View.GONE);
                                        }

                                        btnRegister.setText("SIGN UP");

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {


                                    }
                                }
                        ) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<String, String>();

                                params.put("username", inputUsername.getText().toString());
                                params.put("password", inputPassword.getText().toString());
                                params.put("email", inputEmail.getText().toString());
                                params.put("name", inputName.getText().toString());
                                params.put("phone", "0");
                                params.put("tag", "register");



                                return params;
                            }
                        };

                        requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);
                        //Toast.makeText(getApplicationContext(),"Please wait...Submitting Report",Toast.LENGTH_SHORT).show();
//                        mProgressDialog = new ProgressDialog(getApplicationContext());
//                        // Set progressdialog message
//                        mProgressDialog.setMessage("Loggin In");
//                        mProgressDialog.setIndeterminate(false);
//                        // Show progressdialog
//                        mProgressDialog.show();



                    }

                }
            }
        });

    }
}
