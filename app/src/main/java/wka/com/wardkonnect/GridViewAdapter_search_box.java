package wka.com.wardkonnect;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


public class GridViewAdapter_search_box extends BaseAdapter {


    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    ImageLoader imageLoader;
    HashMap<String, String> resultp = new HashMap<String, String>();

    String get_user;

    String username;


    public GridViewAdapter_search_box(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        imageLoader = new ImageLoader(context);
    }

    static class ViewHolder {

        TextView tvtitle;
        TextView tvprice;
        ImageView imageView;
    }

    ViewHolder holder;

    public View getView(final int position, View gridView, ViewGroup parent) {
        gridView = null;
        resultp = data.get(position);



        if (gridView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView = inflater.inflate(R.layout.gridviewlistitem_square, parent, false);
            // Get the position
            resultp = data.get(position);
            holder = new ViewHolder();

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.gridviewlistitem_square, null);
            holder.imageView = (ImageView) gridView.findViewById(R.id.gridItemImageView);

            gridView.setTag(holder);

        }else{

            holder = (ViewHolder) gridView.getTag();
        }

        Picasso.with(context).load(resultp.get(FragmentSearch.POST_PHOTO)).placeholder(R. mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                .into(holder.imageView);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();


     // final Intent user_profile = new Intent(context, ProfileView.class);
        final Intent main_profile = new Intent(context, SinglePostView.class);

        // Capture ListView item click
        gridView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);

 //    if(resultp.get(SearchActivity.POST_USERNAME).toString().equalsIgnoreCase(username)){

                    main_profile.putExtra("id", resultp.get(FragmentSearch.POST_ID));
                    main_profile.putExtra("username", resultp.get(FragmentSearch.POST_USERNAME));
                    main_profile.putExtra("name", resultp.get(FragmentSearch.POST_NAME));
                    main_profile.putExtra("status", resultp.get(FragmentSearch.POST_STATUS));
                main_profile.putExtra("text", resultp.get(FragmentSearch.POST_TEXT));
                main_profile.putExtra("liked_by", resultp.get(FragmentSearch.POST_LIKED_BY));
                main_profile.putExtra("likes_count", resultp.get(FragmentSearch.POST_LIKES_COUNT));
                main_profile.putExtra("comments_count", resultp.get(FragmentSearch.POST_COMMENTS_COUNT));
                main_profile.putExtra("photo", resultp.get(FragmentSearch.POST_PHOTO));
                main_profile.putExtra("time", resultp.get(FragmentSearch.POST_TIME));
                    main_profile.putExtra("verified", resultp.get(FragmentSearch.POST_VERIFIED));
                    main_profile.putExtra("followers_count", resultp.get(FragmentSearch.POST_FOLLOWERS_CT));
                    main_profile.putExtra("following_count", resultp.get(FragmentSearch.POST_FOLLOWING_CT));
                    main_profile.putExtra("posts_count", resultp.get(FragmentSearch.POST_POSTS_CT));
                    main_profile.putExtra("profile_pic", resultp.get(FragmentSearch.POST_PROFILE_PIC));
                    main_profile.putExtra("profile_banner", resultp.get(FragmentSearch.POST_PROFILE_BANNER));
                main_profile.putExtra("reg_id", resultp.get(FragmentSearch.POST_REG_ID));


//                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
//                //LayoutInflater inflater = context.getLayoutInflater();
//                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                View dialogView = inflater.inflate(R.layout.activity_single_post_view, null);
//                dialogBuilder.setView(dialogView);
//
//                AlertDialog alertDialog = dialogBuilder.create();
//                alertDialog.show();


                    context.startActivity(main_profile);

//                }
//
//                else{
//
//                    user_profile.putExtra("id", resultp.get(SearchActivity.POST_ID));
//                    user_profile.putExtra("username", resultp.get(SearchActivity.POST_USERNAME));
//                    user_profile.putExtra("name", resultp.get(SearchActivity.POST_NAME));
//                    user_profile.putExtra("status", resultp.get(SearchActivity.POST_STATUS));
//                    user_profile.putExtra("verified", resultp.get(SearchActivity.POST_VERIFIED));
//                    user_profile.putExtra("followers_count", resultp.get(SearchActivity.POST_FOLLOWERS_CT));
//                    user_profile.putExtra("following_count", resultp.get(SearchActivity.POST_FOLLOWING_CT));
//                    user_profile.putExtra("posts_count", resultp.get(SearchActivity.POST_POSTS_CT));
//                    user_profile.putExtra("profile_pic", resultp.get(SearchActivity.POST_PROFILE_PIC));
//                    user_profile.putExtra("profile_banner", resultp.get(SearchActivity.POST_PROFILE_BANNER));
//
//
//                    context.startActivity(user_profile);
//                }
//
//
//
//
           }


        });

        return gridView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}