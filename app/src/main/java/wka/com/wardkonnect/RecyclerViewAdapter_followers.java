package wka.com.wardkonnect;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class RecyclerViewAdapter_followers extends RecyclerView.Adapter<RecyclerViewAdapter_followers.ViewHolder> {

    String username;

    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();

    // data is passed into the constructor
    RecyclerViewAdapter_followers(Context context, ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.likes_list_item, parent, false);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();



        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        resultp = data.get(position);

        Picasso.with(context).load(resultp.get(Followers.PROFILE_PIC)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                .into(holder.profilePicImg);
        // Capture position and set results to the TextViews
        holder.usernameTV.setText(resultp.get(Followers.USERNAME));




    }

    // total number of rows
    @Override
    public int getItemCount() {
        return data.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView profilePicImg;
        TextView usernameTV;

        ViewHolder(View itemView) {
            super(itemView);
            // Locate the TextViews in listview_item.xml
            usernameTV = (TextView) itemView.findViewById(R.id.like_username);
            // Locate the ImageView in listview_item.xml
            profilePicImg = (ImageView) itemView.findViewById(R.id.like_dp);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getAdapterPosition();
            resultp = data.get(pos);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    final Intent user_profile = new Intent(context, ProfileView.class);
                    final Intent main_profile = new Intent(context, Profile.class);

                    if(resultp.get(Followers.USERNAME).toString().equalsIgnoreCase(username)){
                        main_profile.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        main_profile.putExtra("id", resultp.get(Followers.ID));
                        main_profile.putExtra("username", resultp.get(Followers.USERNAME));
                        main_profile.putExtra("status", resultp.get(Followers.STATUS));
                        main_profile.putExtra("verified", resultp.get(Followers.VERIFIED));
                        main_profile.putExtra("followers_count", resultp.get(Followers.FOLLOWERS_COUNT));
                        main_profile.putExtra("following_count", resultp.get(Followers.FOLLOWING_COUNT));
                        main_profile.putExtra("posts_count", resultp.get(Followers.POSTS_COUNT));
                        main_profile.putExtra("profile_pic", resultp.get(Followers.PROFILE_PIC));
                        main_profile.putExtra("profile_banner", resultp.get(Followers.PROFILE_BANNER));


                        context.startActivity(main_profile);

                    }

                    else {
                        user_profile.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        user_profile.putExtra("id", resultp.get(Followers.ID));
                        user_profile.putExtra("username", resultp.get(Followers.USERNAME));
                        user_profile.putExtra("status", resultp.get(Followers.STATUS));
                        user_profile.putExtra("verified", resultp.get(Followers.VERIFIED));
                        user_profile.putExtra("followers_count", resultp.get(Followers.FOLLOWERS_COUNT));
                        user_profile.putExtra("following_count", resultp.get(Followers.FOLLOWING_COUNT));
                        user_profile.putExtra("posts_count", resultp.get(Followers.POSTS_COUNT));
                        user_profile.putExtra("profile_pic", resultp.get(Followers.PROFILE_PIC));
                        user_profile.putExtra("profile_banner", resultp.get(Followers.PROFILE_BANNER));


                        context.startActivity(user_profile);

                    }

                }
            });




        }
    }




    @Override
    public long getItemId(int position) {
        return 0;
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        //  this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}