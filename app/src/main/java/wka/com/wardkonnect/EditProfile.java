package wka.com.wardkonnect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class EditProfile extends AppCompatActivity {


    ImageButton back;

    RelativeLayout rel_change_prof_pic;
    RelativeLayout rel_edit_details;
    RelativeLayout rel_edit_profession;
    RelativeLayout rel_edit_region;

    String str_location;
    String str_url;
    String str_status;

    private static final int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        back = (ImageButton) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent sta = getIntent();
        str_status = sta.getStringExtra("status");
        str_location = sta.getStringExtra("location");
        str_url = sta.getStringExtra("url");

        rel_change_prof_pic = (RelativeLayout) findViewById(R.id.rel_change_prof_pic);
        rel_edit_details = (RelativeLayout) findViewById(R.id.rel_edit_details);
        rel_edit_profession = (RelativeLayout) findViewById(R.id.rel_edit_profession);
        rel_edit_region = (RelativeLayout) findViewById(R.id.rel_edit_region);

        rel_change_prof_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(EditProfile.this, ChangeProfilePic.class);
                startActivity(in);
            }
        });


        rel_edit_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent in = new Intent(getApplicationContext(), ChangeProfileDetails.class);
                in.putExtra("status",str_status);
                in.putExtra("location",str_location);
                in.putExtra("url",str_url);
                startActivityForResult(in, REQUEST_CODE);
            }
        });


        rel_edit_profession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(EditProfile.this, EditProfession.class);
                startActivity(in);
            }
        });


        rel_edit_region.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(EditProfile.this, EditRegion.class);
                startActivity(in);
            }
        });

    }
}
