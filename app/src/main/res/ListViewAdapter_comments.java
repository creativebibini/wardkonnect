package com.app.truebelievers;

/**
 * Created by itse-ugcs on 6/14/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

public class ListViewAdapter_comments extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    HashMap<String, String> resultp = new HashMap<String, String>();

    // Declare Variables
    TextView usernameTV;
    ImageView profilePicImg;
    TextView date;
    TextView comment_display;

    String username;

    public ListViewAdapter_comments(Context context,
                                    ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.comments_list_item, parent, false);
        // Get the position
        resultp = data.get(position);

        DatabaseHandler db = DatabaseHandler.getDatabaseHandler(context);
        UserFunctions  userFunctions = new UserFunctions();
        userFunctions.isUserLoggedIn(context);
        // user already logged in show dash board
        username = db.getUsername();

        // Locate the TextViews in listview_item.xml
        usernameTV = (TextView) itemView.findViewById(R.id.comment_username);
        comment_display = (TextView) itemView.findViewById(R.id.comment_comment);
        date = (TextView) itemView.findViewById(R.id.comment_date);
        // Locate the ImageView in listview_item.xml
        profilePicImg = (ImageView) itemView.findViewById(R.id.comment_dp);

        // Capture position and set results to the TextViews

       // comment_display.setText(resultp.get(Comments.COMMENT_COMMENT));

        comment_display.setText(AndroidEmoji.ensure(resultp.get(HomePostsComments.COMMENT_COMMENT), context));


        Picasso.with(context).load(resultp.get(HomePostsComments.COMMENT_PROFILE_PIC)).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.default_back_ash)
                .into(profilePicImg);

        // Capture position and set results to the TextViews
        usernameTV.setText(resultp.get(HomePostsComments.COMMENT_USERNAME));

        date.setText(resultp.get(HomePostsComments.COMMENT_DATE));

        final Intent user_profile = new Intent(context, ProfileView.class);
        final Intent main_profile = new Intent(context, Profile.class);
        // Capture ListView item click
        itemView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);
                if(resultp.get(HomePostsComments.COMMENT_USERNAME).toString().equalsIgnoreCase(username)){
                    main_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    main_profile.putExtra("id", resultp.get(HomePostsComments.ID));
                    main_profile.putExtra("username", resultp.get(HomePostsComments.COMMENT_USERNAME));
                    main_profile.putExtra("name", resultp.get(HomePostsComments.COMMENT_NAME));
                    main_profile.putExtra("status", resultp.get(HomePostsComments.COMMENT_STATUS));
                    main_profile.putExtra("verified", resultp.get(HomePostsComments.COMMENT_VERIFIED));
                    main_profile.putExtra("followers_count", resultp.get(HomePostsComments.COMMENT_FOLLOWERS_COUNT));
                    main_profile.putExtra("following_count", resultp.get(HomePostsComments.COMMENT_FOLLOWING_COUNT));
                    main_profile.putExtra("posts_count", resultp.get(HomePostsComments.COMMENT_POSTS_COUNT));
                    main_profile.putExtra("profile_pic", resultp.get(HomePostsComments.COMMENT_PROFILE_PIC));
                    main_profile.putExtra("profile_banner", resultp.get(HomePostsComments.COMMENT_PROFILE_BANNER));

                    context.startActivity(main_profile);
                }

                else{
                    user_profile.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    user_profile.putExtra("id", resultp.get(HomePostsComments.ID));
                    user_profile.putExtra("username", resultp.get(HomePostsComments.COMMENT_USERNAME));
                    user_profile.putExtra("name", resultp.get(HomePostsComments.COMMENT_NAME));
                    user_profile.putExtra("status", resultp.get(HomePostsComments.COMMENT_STATUS));
                    user_profile.putExtra("verified", resultp.get(HomePostsComments.COMMENT_VERIFIED));
                    user_profile.putExtra("followers_count", resultp.get(HomePostsComments.COMMENT_FOLLOWERS_COUNT));
                    user_profile.putExtra("following_count", resultp.get(HomePostsComments.COMMENT_FOLLOWING_COUNT));
                    user_profile.putExtra("posts_count", resultp.get(HomePostsComments.COMMENT_POSTS_COUNT));
                    user_profile.putExtra("profile_pic", resultp.get(HomePostsComments.COMMENT_PROFILE_PIC));
                    user_profile.putExtra("profile_banner", resultp.get(HomePostsComments.COMMENT_PROFILE_BANNER));

                    context.startActivity(user_profile);
                }


            }
        });

        return itemView;
    }



}



