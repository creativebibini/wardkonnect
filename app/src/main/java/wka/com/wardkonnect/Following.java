package wka.com.wardkonnect;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Following extends AppCompatActivity {

    ImageButton back;

    String sch;

    static String ID = "id";
    static String USERNAME = "username";
    static String POSTS_COUNT = "posts_count";
    static String STATUS = "status";
    static String VERIFIED = "verified";
    static String FOLLOWING_COUNT = "following_count";
    static String FOLLOWERS_COUNT = "followers_count";
    static String PROFILE_PIC = "profile_pic";
    static String PROFILE_BANNER = "profile_banner";

    //int current_page = 1;
    //Button btnLoadMore;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ListViewAdapter_following adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;

    ListView listview;

    LinearLayout linlaHeaderProgress;

    RequestQueue requestQueue;
    StringRequest stringRequest;
    public static final String TAG = "CommPost";

    String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);

        back = (ImageButton) findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Intent i = getIntent();
        user = i.getStringExtra("username");

//        Toast.makeText(getApplicationContext(), user, Toast.LENGTH_SHORT).show();

        back = (ImageButton)findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        listview = (ListView)findViewById(R.id.followingLV);

        // Creating a button - Load More
//        btnLoadMore = new Button(Following.this);
//        btnLoadMore.setText("Load More");
//        btnLoadMore.setBackgroundDrawable(null);

        // Adding button to listview at footer
   //     listview.addFooterView(btnLoadMore);

//        btnLoadMore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View arg0) {
//
//
//                ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
//                if (!cd.isConnectingToInternet()) {
//
//                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
//
//                    return;
//                }
//                else
//                {
//
//                    sendRequestMore();
//
//                }
//
//
//            }
//        });

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {

            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();

            return;
        }


        else
        {
            sendRequest();
        }

    }

    private void sendRequest(){



        String JSON_URL = "http://wardkonnect.xyz/app/follow/get_following.php?user="+user+"&page=1";
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
                linlaHeaderProgress.setVisibility(View.GONE);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Following.this,"Internet connection is down",Toast.LENGTH_LONG).show();
                        linlaHeaderProgress.setVisibility(View.GONE);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

        linlaHeaderProgress.setVisibility(View.VISIBLE);
    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray("following");

            for (int i = 0; i < jsonarray.length(); i++) {

                HashMap<String, String> map = new HashMap<String, String>();
                jsonobject = jsonarray.getJSONObject(i);
                // Retrieve JSON Objects
                map.put("id", jsonobject.getString("id"));
                map.put("username", jsonobject.getString("username"));
                map.put("status", jsonobject.getString("status"));
                map.put("verified", jsonobject.getString("verified"));
                map.put("posts_count", jsonobject.getString("posts_count"));
                map.put("followers_count", jsonobject.getString("followers_count"));
                map.put("following_count", jsonobject.getString("following_count"));
                map.put("profile_pic", jsonobject.getString("profile_pic"));
                map.put("profile_banner", jsonobject.getString("profile_banner"));
                // Set the JSON Objects into the array



                arraylist.add(map);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        adapter = new ListViewAdapter_following(Following.this, arraylist);
        // Set the adapter to the ListView
        listview.setAdapter(adapter);
    }

//    private void sendRequestMore(){
//
//        current_page++;
//
//        String JSON_URL = "http://truebelieversworld.com/tb/follow/get_following.php?user="+"pdee"+"&"+"page="+current_page;
//        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                showJSONMore(response);
//                linlaHeaderProgress.setVisibility(View.GONE);
//            }
//        },
//
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(Following.this,"Internet connection is down",Toast.LENGTH_LONG).show();
//                        linlaHeaderProgress.setVisibility(View.GONE);
//                    }
//                });
//
//        stringRequest.setTag(TAG);
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
//
//        requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
//
//        btnLoadMore.setText("Loading ...");
//    }
//
//
//    private void showJSONMore(String json){
//        jsonobject = null;
//        arraylist = new ArrayList<HashMap<String, String>>();
//        try {
//            jsonobject = new JSONObject(json);
//            jsonarray = jsonobject.getJSONArray("following");
//
//            for (int i = 0; i < jsonarray.length(); i++) {
//
//                HashMap<String, String> map = new HashMap<String, String>();
//                jsonobject = jsonarray.getJSONObject(i);
//                // Retrieve JSON Objects
//                map.put("id", jsonobject.getString("id"));
//                map.put("username", jsonobject.getString("username"));
//                map.put("status", jsonobject.getString("status"));
//                map.put("verified", jsonobject.getString("verified"));
//                map.put("posts_count", jsonobject.getString("posts_count"));
//                map.put("followers_count", jsonobject.getString("followers_count"));
//                map.put("following_count", jsonobject.getString("following_count"));
//                map.put("profile_pic", jsonobject.getString("profile_pic"));
//                map.put("profile_banner", jsonobject.getString("profile_banner"));
//                // Set the JSON Objects into the array
//
//
//
//                arraylist.add(map);
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        adapter = new ListViewAdapter_following(Following.this, arraylist);
//       // int currentPosition = listview.getFirstVisiblePosition();
//        listview.setAdapter(adapter);
//        // Setting new scroll position
//      //  listview.setSelectionFromTop(currentPosition + 1, 0);
//        //linlaHeaderProgress.setVisibility(View.GONE);
//       // btnLoadMore.setText("Load More");
//    }
}
