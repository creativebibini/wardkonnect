package wka.com.wardkonnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.test.mock.MockPackageManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.messaging.FirebaseMessaging;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    TabLayout tabs;

    UserFunctions userFunctions;

    String str_id;
    String str_username;
    String str_name;
    String str_location;
    String str_url;
    String str_followersct;
    String str_followingct;
    String str_postsct;
    String str_status;
    String str_profile_picture;
    String str_profile_banner;
    String str_profile_verified;

    static String PROFILE = "profile";
    static String PROFILE_ID = "id";
    static String PROFILE_STATUS = "status";
    static String PROFILE_FOLLOWERS = "followers_count";
    static String PROFILE_FOLLOWING = "following_count";
    static String PROFILE_POSTS = "posts_count";
    static String PROFILE_PICTURE = "profile_picture";
    static String PROFILE_BANNER = "profile_banner";


    TextView my_profile_followers_count;
    TextView my_profile_following_count;
    ImageView my_profile_profile_pic;

    TextView my_profile_name;
    TextView my_profileLocation;
    TextView my_profile_profilebio;
    TextView my_profileLink;

    JSONObject jsonobject;
    JSONArray jsonarray;
    ArrayList<HashMap<String, String>> arraylist;

    RequestQueue requestQueue;
    StringRequest stringRequest;

    public static final String TAG = "MyTag";

    private static final int REQUEST_CODE = 100;

    LinearLayout linlaHeaderProgress;

    public static String username;

    private int[] tabIcons = {
            R.drawable.ic_action_name_home,
            R.drawable.ic_action_name_search,
            R.drawable.ic_action_name_notifications,
            R.drawable.ic_action_name_messages
    };


    FloatingActionButton post;
  ImageButton settings;
  ImageView profile;

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    private static final int REQUEST_CODE_PERMISSION = 2;
    List<String> mPermission=new ArrayList<String>();

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    AsyncTask<Void, Void, Void> mRegisterTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userFunctions = new UserFunctions();
        if(userFunctions.isUserLoggedIn(getApplicationContext())){


        setContentView(R.layout.activity_main);

            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    // checking for type intent filter
                    if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                        // gcm successfully registered
                        // now subscribe to `global` topic to receive app wide notifications
                        FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                        displayFirebaseRegId();

                    } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                        // new push notification is received

                        String message = intent.getStringExtra("message");

                        Toast.makeText(getApplicationContext(),  message, Toast.LENGTH_LONG).show();

                        //txtMessage.setText(message);
                    }
                }
            };

            requestPermission();

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

         settings = (ImageButton) findViewById(R.id.settings);
         profile = (ImageView) findViewById(R.id.profile);


            my_profile_profile_pic  = (ImageView) findViewById(R.id.profile);

            DatabaseHandler db = DatabaseHandler.getDatabaseHandler(getApplicationContext());
            UserFunctions  userFunctions = new UserFunctions();
            userFunctions.isUserLoggedIn(getApplicationContext());
            // user already logged in show dash board
            username = db.getUsername();

            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            if (!cd.isConnectingToInternet()) {

                Toast.makeText(getApplicationContext(),"No Internet Connection", Toast.LENGTH_SHORT).show();


            }
            else
            {

                sendRequest();
                PostRegId();

            }


        // Set Tabs inside Toolbar
        tabs = (TabLayout) findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
        setupTabIcons();

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(MainActivity.this, Settings.class);
                startActivity(in);

            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in = new Intent(MainActivity.this, Profile.class);
                in.putExtra("username",username);
                startActivity(in);

            }
        });


        }else{
            // user is not logged in show login screen
            Intent welcome = new Intent(getApplicationContext(), WelcomeScreen.class);
            welcome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(welcome);
            // Closing dash board screen
            finish();
        }



}


    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {


        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentHome(), "Home");
        adapter.addFragment(new FragmentSearch(), "Search");
        adapter.addFragment(new FragmentNotifications(), "Notifications");
        adapter.addFragment(new FragmentMessage(), "Message");
        viewPager.setAdapter(adapter);



    }

    private void setupTabIcons() {
        tabs.getTabAt(0).setIcon(tabIcons[0]);
        tabs.getTabAt(1).setIcon(tabIcons[1]);
        tabs.getTabAt(2).setIcon(tabIcons[2]);
        tabs.getTabAt(3).setIcon(tabIcons[3]);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }



    }



    private void sendRequest(){


        String JSON_URL = "http://wardkonnect.xyz/app/profile/get_prof_details.php?username="+username;
        stringRequest = new StringRequest(JSON_URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showJSON(response);
            }
        },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        requestQueue.cancelAll(TAG);
                        Toast.makeText(getApplicationContext(), "Internet connection is down", Toast.LENGTH_LONG).show();


                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        //RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        requestQueue = Volley.newRequestQueue(this);
        DiskBasedCache cache = new DiskBasedCache(getCacheDir(), 100 * 1024 * 1024);
        requestQueue = new RequestQueue(cache, new BasicNetwork(new HurlStack()));
        requestQueue.start();
        requestQueue.add(stringRequest);



    }


    private void showJSON(String json){
        jsonobject = null;
        arraylist = new ArrayList<HashMap<String, String>>();
        try {
            jsonobject = new JSONObject(json);
            jsonarray = jsonobject.getJSONArray(PROFILE);

            JSONObject c = jsonarray.getJSONObject(0);
            // Storing  JSON item in a Variable
            str_id = c.getString("id");
            str_username = c.getString("username");
            str_name = c.getString("name");
            str_status = c.getString("status");
            str_location = c.getString("location");
            str_url = c.getString("url");
            str_followersct = c.getString("followers_count");
            str_followingct = c.getString("following_count");
            str_postsct = c.getString("posts_count");
            str_profile_picture = c.getString("profile_picture");
            str_profile_banner = c.getString("profile_banner");
            str_profile_verified = c.getString("verified");





        } catch (JSONException e) {
            e.printStackTrace();
        }

        Picasso.with(this).load(str_profile_picture).placeholder(R.mipmap.default_back_ash).fit().centerCrop().error(R.mipmap.placeholder)
                .into(my_profile_profile_pic);




    }

    public void requestPermission()
    {
        try {

            if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                    != MockPackageManager.PERMISSION_GRANTED)
                mPermission.add(android.Manifest.permission.CAMERA);

            if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != MockPackageManager.PERMISSION_GRANTED


                    )
                mPermission.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE

                );

            if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != MockPackageManager.PERMISSION_GRANTED)
                mPermission.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);




            if(mPermission.size()>0)

            {
                String[] array = mPermission.toArray(new String[mPermission.size()]);
                ActivityCompat.requestPermissions(this, array, REQUEST_CODE_PERMISSION);

                // If any permission aboe not allowed by user, this condition will execute every tim, else your else part will work
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length == mPermission.size())             {
                for(int i=0;i<grantResults.length;i++)
                {
                    if(grantResults[i] == MockPackageManager.PERMISSION_GRANTED)
                    {
                      //  Toast.makeText(this,"Permission has been granted",Toast.LENGTH_LONG).show();
                    }
                    else{
                        mPermission=new ArrayList<String>();
                        requestPermission();
                        break;
                    }
                }

            }
            else{
               // Toast.makeText(this,"Permission has been granted",Toast.LENGTH_LONG).show();
                mPermission=new ArrayList<String>();
                requestPermission();
            }
        }

    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

          if (!TextUtils.isEmpty(regId))
          // txtRegId.setText("Firebase Reg Id: " + regId);
          Toast.makeText(this,regId,Toast.LENGTH_LONG).show();
           else

            Toast.makeText(this,"Firebase Reg Id is not received yet!",Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    private void PostRegId() {


        stringRequest = new StringRequest(Request.Method.POST, "http://wardkonnect.xyz/app/firebase/register_regid.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                        //    Toast.makeText(getApplicationContext(),"Comment Added",Toast.LENGTH_SHORT).show();



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                String regId = pref.getString("regId", null);


                params.put("username", username);
                params.put("regid", regId);

                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        //Toast.makeText(getApplicationContext(),"Please wait...Adding Comment",Toast.LENGTH_SHORT).show();



    }

}
